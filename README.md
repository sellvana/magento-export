## About
This is a Magento extension which only purpose is to provide a way to export data
in format suitable for import into [Sellvana](https://sellvana.com).
It is useful for those who consider moving to Sellvana or just want to play around with it using their own data.

The extension is compatible with Magento 1.6+ and PHP 5.4+
If your store uses some special national characters (something like å, ā, ø, ķ),
it is recommended to have **intl** extension for PHP installed for transliteration process to work correctly.

## Installation
1. Disable compilation if it is enabled (System -> Tools -> Compilation)
2. Disable cache if it is enabled (System -> Cache Management)
3. [Download](https://bitbucket.org/etws/sellvana-magento-export/downloads) the extension
4. Copy all files from the "install" folder to the Magento root folder - where your index.php is
5. Log out from the admin panel
6. Log in to the admin panel with your login and password
7. Run the compilation process and enable cache if needed

## Creating an export profile
Before you can export data, you need to create export profile. This is a pretty straight forward process:
1. Navigate to System -> Import/Export -> Export to Sellvana
2. Press Add New Profile
3. Enter filename and select entities you want to export

## Running a profile
Here you have 3 options. Feel free to try any of them and see if it works for you.

#### Running from web
*NOTE: This option is as simple and easy to use as it can be, but it depends on settings and performance of your hosting.
If you have a lot of products in your store and see a blank or error page when you try to run export,
you should probably try other options.*

1. Navigate to System -> Import/Export -> Export to Sellvana
2. Select a Generate action for the selected profile
3. After export is finished, you will be able to download an export file by selecting "Download export file"
near export profile, or download it manually from **var/export** folder.

#### Running by cron
*NOTE: This options requires that cron is set up on your server.*

1. Navigate to System -> Import/Export -> Export to Sellvana
2. Select a Run on Background action for the selected profile
3. Wait for notification email being sent to your email (General contact)
3. Download an export file by selecting "Download export file"
near export profile, or download it manually from **var/export** folder.

#### Running from console
*NOTE: This is a most flexible option, but it requires a SSH access to the server*

1. Go to the root folder of your website (where your index.php is)
2. Execute command ```
php -f shell/SellvanaExport.php export
```
3. Select a profile from the list
4. Wait for export to finish
5. Download export file manually from **var/export** folder.


## Additional information:

* When exporting Website|Store|Store View, the extension doesn't consider situations when in the same Store Views
(e.g. if Store View is a website language) in the different Stores products have different attribute values.
In this case values will be taken from the first Store View.
* At this moment extension doesn't consider url_key override on Store View level - default value is used.
The reason is that in Sellvana url_key and url_path don't depend on the Website.

## Things to think about in the future:
1.  At this moment there is no possible way to influence Sellvana settings using import.
That means there are following problems:

* Impossible to override default root category
* Impossible to set default or website currency