<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Alternative of Mage_Shell_Abstract::_validate, uses less memory
 */
if (ini_get('register_argc_argv')) {
    if (null === $argc) {
        exit('This script cannot be run from Browser. This is the shell script.');
    }
} elseif (array_key_exists('REMOTE_ADDR', $_SERVER)) {
    exit('This script cannot be run from Browser. This is the shell script.');
}

require_once 'abstract.php';

/**
 * Class Mage_Shell_SellvanaExport
 */
class Mage_Shell_SellvanaExport extends Mage_Shell_Abstract
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        /** @var Sellvana_SellvanaExport_Helper_Shell $shellHelper */
        $shellHelper = Mage::helper('sellvana_sellvanaexport/shell');

        /** @var Sellvana_SellvanaExport_Model_Profile $profile */
        $profile = Mage::getModel('sellvana_sellvanaexport/profile');

        try {
            if ($this->getArg('info')) {
                $shellHelper->stdout(" ");
                $shellHelper->stdout('{green}List of allowed file names:{/}');

                /** @var Sellvana_SellvanaExport_Model_Resource_Profile_Collection $collection */
                $collection = $profile->getCollection();
                $i = 1;
                /** @var Sellvana_SellvanaExport_Model_Profile $file */
                foreach ($collection as $file) {
                    $shellHelper->stdout('  [' . $i++ . '] {^purple}' . str_pad($file->getFile() . '{/}', 20));
                }
                $shellHelper->stdout(" ");

            } elseif ($this->getArg('export')) {
                $fileName = $this->getArg('export');
                if ($fileName === true) {
                    $shellHelper->stdout(" ");
                    $shellHelper->stdout('{green}Please select filename from allowed:{/}');

                    /** @var Sellvana_SellvanaExport_Model_Resource_Profile_Collection $collection */
                    $collection = $profile->getCollection();
                    $ids = array();
                    $i = 1;

                    /** @var Sellvana_SellvanaExport_Model_Profile $file */
                    foreach ($collection as $file) {
                        $ids[$i] = $file->getID();
                        $shellHelper->stdout('  [' . $i++ . '] {^purple}' . str_pad($file->getFile() . '{/}', 20));
                    }

                    $fileId = null;
                    while (true) {
                        $shellHelper->stdout('{yellow}Filename id: {/}', false, '');
                        $fileId = $shellHelper->stdin();
                        if (array_key_exists($fileId, $ids)) {
                            break;
                        }
                    }

                    $fileProfile = $profile->load($fileId);

                } else {
                    $fileProfile = $profile->load($fileName, 'file');

                    if (!$fileProfile->getId()) {
                        Mage::throwException('Unknown file for export');
                    }
                }
                /** @var Sellvana_SellvanaExport_Model_Profile $fileProfile */
                if ($fileProfile->isRunning() && !$this->getArg('force')) {
                    Mage::throwException('The profile is already running. You can force re-running it with {white}--force{/} {purple}flag, if you are sure you need to.');
                }

                /** @var Sellvana_SellvanaExport_Model_Export $export */
                $export = Mage::getModel("sellvana_sellvanaexport/export", array('profile' => $fileProfile));

                Sellvana_SellvanaExport_Helper_Debug::$echoType = 'console';
                $export->export();

            } else {
                $this->usageHelp();
            }
        } catch (Exception $e) {
            $shellHelper->stdout("\n{red}ERROR:{/} {purple}" . $e->getMessage() . "\n");
        }
    }

    /**
     * @inheritdoc
     */
    public function usageHelp()
    {
        $usage = <<<USAGE
{green}Usage:  {/}php -f SellvanaExport.php -- [options]

  {^purple}--export {/}{purple}<filename>      {/}Export data to file.

  {^purple}export                   {/}Export data, will ask file.
  {^purple}info                     {/}Show allowed files.
  {^purple}help                     {/}This help
USAGE;
        $shellHelper = Mage::helper('sellvana_sellvanaexport/shell');
        $shellHelper->stdout($usage);
        die();
    }
}

$shell = new Mage_Shell_SellvanaExport();
$shell->run(); 