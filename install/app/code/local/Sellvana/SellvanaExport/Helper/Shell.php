<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Boris Gurvich <boris@sellvana.com>
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Helper_Shell
 */
class Sellvana_SellvanaExport_Helper_Shell extends Mage_Core_Helper_Abstract
{
    /**
     * @var array String foreground and background colors.
     */
    protected $_colorCodes = [
        'normal' => '0',
        'reset' => '0',
        'bold' => '1',
        'underline' => '4',
        'light' => '5',
        'inverse' => '7',

        'black' => '30',
        'red' => '31',
        'green' => '32',
        'yellow' => '33',
        'blue' => '34',
        'purple' => '35',
        'cyan' => '36',
        'white' => '37',

        'bg-black' => '40',
        'bg-red' => '41',
        'bg-green' => '42',
        'bg-yellow' => '43',
        'bg-blue' => '44',
        'bg-purple' => '45',
        'bg-cyan' => '46',
        'bg-white' => '47',
    ];

    /**
     * @var array String styling.
     */
    protected $_shortMods = [
        '.' => '0', // normal
        '/' => '0', // reset (normal)
        '*' => '1', // bold
        '_' => '4', // underline
        '^' => '5', // light background
        '!' => '7', // inverse front and back colors
    ];

    /**
     * Calculated colors regex
     *
     * @var string
     */
    protected $_colorsRegex;

    /**
     * Use colors
     *
     * @var bool
     */
    protected $_colorsEnabled;

    /**
     * Sellvana_SellvanaExport_Helper_Shell constructor.
     */
    public function __construct()
    {
        $this->_colorsEnabled = strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN';

        if (extension_loaded('posix') && !posix_isatty(STDOUT)) {
            $this->_colorsEnabled = false;
        }

        // calculate colors regex
        $mods = [];
        foreach ($this->_shortMods as $m => $_) {
            $mods[] = preg_quote($m, '#');
        }
        $modsRe = '[' . join('', $mods) . ']*';
        $colorsRe = '(' . join('|', array_keys($this->_colorCodes)) . ')';

        $this->_colorsRegex = "#\\{({$modsRe})({$colorsRe}(;{$colorsRe})*)?({$modsRe})\\}#";
    }

    /**
     * Enable the use of coloring
     *
     * @return bool
     */
    public function enableColors()
    {
        $this->_colorsEnabled = strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN';

        if (extension_loaded('posix') && !posix_isatty(STDOUT)) {
            $this->_colorsEnabled = false;
            return false;
        }

        return true;
    }

    /**
     * Disable the use of coloring
     */
    public function disableColors()
    {
        $this->_colorsEnabled = false;
    }

    /**
     * Colorize a string for shell output
     *
     * Format examples:
     *      "{black;bg-white}Example{/}" - black on white
     *      "{yellow*}Example{/}" - yellow bold
     *      "{yellow;bold}Example{/}" - yellow bold
     *      "{_*blue;bg-red^}Example{/}" - blue bold and underscored on a bright red
     *
     * @param $string
     * @return mixed
     */
    public function colorize($string)
    {
        return preg_replace_callback($this->_colorsRegex, function($m) {
            if (!$this->_colorsEnabled) {
                return '';
            }
            $colors = [];
            if ($m[1]) {
                foreach (str_split($m[1]) as $c) {
                    $colors[] = $this->_shortMods[$c];
                }
            }
            if ($m[2]) {
                foreach (explode(';', $m[2]) as $c) {
                    $colors[] = $this->_colorCodes[$c];
                }
            }
            if (!empty($m[6])) {
                foreach (str_split($m[6]) as $c) {
                    $colors[] = $this->_shortMods[$c];
                }
            }
            return "\033[" . join(';', $colors) . "m";
        }, $string);
    }

    /**
     * Strip a string of ansi-control codes.
     *
     * @param string $string String to strip
     * @return string
     */
    public function strip($string)
    {
        return preg_replace('/\033\[(\d+)(;\d+)*m/', '', $string);
    }

    /**
     * Get from STDIN
     *
     * @param bool $raw If set to true, returns the raw string without trimming
     * @return string
     */
    public function stdin($raw = false)
    {
        return $raw ? fgets(STDIN) : rtrim(fgets(STDIN), PHP_EOL);
    }

    /**
     * Print to STDOUT.
     *
     * @param string $string
     * @param bool $raw
     * @param string $eol
     * @return int
     */
    public function stdout($string, $raw = false, $eol = PHP_EOL)
    {
        $string .= $eol;
        if ($raw) {
            return fwrite(STDOUT, $string);
        }  else {
            return fwrite(STDOUT, $this->colorize($string));
        }
    }
}