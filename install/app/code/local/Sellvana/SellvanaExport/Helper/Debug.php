<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Helper_Debug
 */
class Sellvana_SellvanaExport_Helper_Debug extends Mage_Core_Helper_Abstract
{
    const DEBUG = true;
    const APP_GLOBAL_KEY = "global";

    protected static $_times = array();

    public static $echoType = 'normal';

    /**
     * @param string $key
     * @param bool|false $echo
     */
    public function start($key = self::APP_GLOBAL_KEY, $echo = false)
    {
        if (!self::DEBUG) {
            return;
        }
        self::$_times[$key]['startTime'] = microtime(true);

        if ($echo) {
            if (self::$echoType == 'console') {
                /** @var Sellvana_SellvanaExport_Helper_Shell $shellHelper */
                $shellHelper = Mage::helper('sellvana_sellvanaexport/shell');

                $shellHelper->stdout('Process {purple}"' . $key . '"{/}', false, '');
            } else {
                echo 'Process "' . $key;
            }
        }
    }

    /**
     * @param string $key
     * @param bool|false $echo
     */
    public function end($key = self::APP_GLOBAL_KEY, $echo = false)
    {
        if (!self::DEBUG) {
            return;
        }

        if (!array_key_exists('endTime', self::$_times[$key])) {
            self::$_times[$key]['endTime'] = microtime(true);
            self::$_times[$key]['memory'] = self::convert(memory_get_usage());
        }

        $time = sprintf("%.2F", (self::$_times[$key]['endTime'] - self::$_times[$key]['startTime'])) . "s";

        if ($echo) {
            if (self::$echoType == 'console') {
                /** @var Sellvana_SellvanaExport_Helper_Shell $shellHelper */
                $shellHelper = Mage::helper('sellvana_sellvanaexport/shell');

                $shellHelper->stdout(' Time: {green}' . $time
                    . "{/} Memory: {green}" . self::$_times[$key]['memory'] . '{/}');
            } else {
                echo ' Time: ' . $time . " Memory: " . self::$_times[$key]['memory'] . "\n";
            }
        }
    }

    /**
     * @param array $keys
     */
    public function total(array $keys)
    {
        if (!self::DEBUG) {
            return;
        }
        if (self::$echoType == 'console') {
            $this->renderConsoleTotal($keys);
            return;
        }
        echo "\n\ntotals:\n";
        echo "----------------\n";
        foreach (self::$_times as $key => $value ) {
            if (in_array($key, $keys)) {
                $this->end($key, true);
            }
        }
        echo "----------------\n";
    }

    /**
     * @param $size
     * @return string
     */
    public function convert($size)
    {
        $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
        $exponent = (int)floor(log($size, 1024));
        return @round($size / pow(1024, $exponent), 2) . ' ' . $unit[$exponent];
    }

    /**
     * @param array $keys
     */
    public function renderConsoleTotal(array $keys)
    {
        /** @var Sellvana_SellvanaExport_Helper_Shell $shellHelper */
        $shellHelper = Mage::helper('sellvana_sellvanaexport/shell');

        $keysToDisplay = array_intersect(
            $keys,
            array_keys(self::$_times)
        );

        $maxLength = 0;
        foreach ($keysToDisplay as $key) {
            $maxLength = strlen($key) > $maxLength ? strlen($key) : $maxLength;
        }
        $maxLength += 4;
        $shellHelper->stdout('');
        $title = "| " .str_pad('Model Name', $maxLength) . " | " . str_pad('Time', 12)
            . " | " . str_pad('Memory', 12) . " |";

        $header = "|-" .str_pad('-', $maxLength, '-') . "-|-" . str_pad('-', 12, '-')
            . "-|-" . str_pad('-', 12, '-') . "-|";

        $shellHelper->stdout($header);
        $shellHelper->stdout($title);
        $shellHelper->stdout($header);

        foreach ($keysToDisplay as $key) {
            if ($key == 'global') {
                $shellHelper->stdout($header);
            }
            $this->end($key);
            $time = sprintf("%.2F", (self::$_times[$key]['endTime'] - self::$_times[$key]['startTime'])) . "s";
            $line = "| {purple}" .str_pad($key, $maxLength) . "{/} | {green}" . str_pad($time, 12)
                . "{/} | {green}" . str_pad(self::$_times[$key]['memory'], 12) . "{/} |";
            $shellHelper->stdout($line);
        }
        $shellHelper->stdout($header);
    }
}