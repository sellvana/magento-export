<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Block_Adminhtml_Profile_Grid
 */
class Sellvana_SellvanaExport_Block_Adminhtml_Profile_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('grid_id');
        $this->setDefaultSort('profile_id');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('sellvana_sellvanaexport/profile')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @inheritdoc
     */
    protected function _prepareColumns()
    {
        $this->addColumn('profile_id',
            array(
                'header' => $this->__('ID'),
                'width' => '50px',
                'index' => 'profile_id'
            )
        );

        $this->addColumn('file',
            array(
                'header' => $this->__('File'),
                'index' => 'file'
            )
        );

        $this->addColumn('last_success',
            array(
                'header' => $this->__('Last Success'),
                'index' => 'last_success',
                'type' => 'datetime'
            )
        );

        $this->addColumn('last_response',
            array(
                'header' => $this->__('Last Response'),
                'index' => 'last_response',
                'type' => 'datetime',
            )
        );

        $this->addColumn('run_status',
            array(
                'header' => $this->__('Status'),
                'index' => 'run_status',
                'type' => 'options',
                'options' => Mage::getModel('sellvana_sellvanaexport/system_config_source_profileStatus')->toOptionArray(),
                'width' => 120,
            )
        );

        $this->addColumn('action',
            array(
                'header'    => $this->__('Action'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'     => 'getId',
                'actions'   => array(
                    array(
                        'caption' => $this->__('Generate'),
                        'url'     => array(
                            'base'=>'*/*/generate',
                        ),
                        'field'   => 'id'
                    ),
                    array(
                        'caption' => $this->__('Run in background (cron)'),
                        'url'     => array(
                            'base'=>'*/*/schedule',
                        ),
                        'field'   => 'id'
                    ),
                    array(
                        'caption' => $this->__('Download export file'),
                        'url'     => array(
                            'base'=>'*/*/getExportFile',
                        ),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
            )
        );

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));

        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    /**
     * @inheritdoc
     * @param Mage_Catalog_Model_Product|Varien_Object $row
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('sellvana_sellvanaexport/profile')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
}
