<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Block_Adminhtml_Profile_Edit_Form
 */
class Sellvana_SellvanaExport_Block_Adminhtml_Profile_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * @return mixed
     */
    protected function _getModel()
    {
        return Mage::registry('current_profile');
    }

    /**
     * @return Sellvana_SellvanaExport_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('sellvana_sellvanaexport');
    }

    /**
     * @return string
     */
    protected function _getModelTitle()
    {
        return 'Profile';
    }

    /**
     * @inheritdoc
     */
    protected function _prepareForm()
    {
        $model = $this->_getModel();
        $modelTitle = $this->_getModelTitle();
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save'),
            'method' => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => $this->_getHelper()->__("$modelTitle Information"),
            'class' => 'fieldset-wide',
        ));

        $fieldset->addField('file', 'text', array(
            'name' => 'file',
            'label' => $this->_getHelper()->__('Filename'),
            'title' => $this->_getHelper()->__('Filename'),
            'required' => true,
        ));

        /** @var Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups $modelGroups */
        $modelGroups = Mage::getModel('sellvana_sellvanaexport/system_config_source_modelGroups');
        $fieldset->addField('entities', 'multiselect', array(
            'name' => 'entities',
            'label' => $this->_getHelper()->__('Entities to export'),
            'title' => $this->_getHelper()->__('Entities to export'),
            'required' => true,
            'values' => $modelGroups->toOptionArray()
        ));

        $path = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'var/export/' . $model->getFile();

        if ($model && $model->getId()) {
            $modelPk = $model->getResource()->getIdFieldName();
            $fieldset->addField($modelPk, 'hidden', array(
                'name' => $modelPk,
            ));

            $fieldset->addField('path', 'link', array(
                'label' => $this->_getHelper()->__('Path to the file'),
                'title' => $this->_getHelper()->__('Path to the file'),
                'required' => true,
                'type' => 'label',
                'href' => $path
            ));
        }

        if ($model) {
            $values = $model->getData();
            $values['path'] = $path;
            $form->setValues($values);
        }
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
