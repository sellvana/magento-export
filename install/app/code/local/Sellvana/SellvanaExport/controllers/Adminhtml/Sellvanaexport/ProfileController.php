<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Adminhtml_Sellvanaexport_ProfileController
 */
class Sellvana_SellvanaExport_Adminhtml_Sellvanaexport_ProfileController extends Mage_Adminhtml_Controller_Action
{
    const CRON_STRING_PATH = 'crontab/jobs/sellvanaexport_generate_profile_%s/schedule/cron_expr';
    const CRON_MODEL_SOURCE_PATH = 'crontab/jobs/sellvana_sellvanaexport/run/model';
    const CRON_MODEL_DEST_PATH = 'crontab/jobs/sellvanaexport_generate_profile_%s/run/model';

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->_forward('grid');
    }

    /**
     * Grid action
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * New action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Edit action
     */
    public function editAction()
    {
        $profileId = $this->getRequest()->getParam('id');
        $profile = Mage::getModel('sellvana_sellvanaexport/profile');

        if ($profileId) {
            $profile->load($profileId);
            if (!$profile->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('sellvana_sellvanaexport')->__('This profile does not exist.')
                );
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!$data) {
            $profile->setData($data);
        }

        Mage::register('current_profile', $profile);

        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Save action
     */
    public function saveAction()
    {
        $redirectBack = $this->getRequest()->getParam('back', false);
        if ($data = $this->getRequest()->getPost()) {
            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('sellvana_sellvanaexport/profile');
            $helper = Mage::helper('sellvana_sellvanaexport');
            $session = $this->_getSession();
            if ($id) {
                $model->load($id);
                if (!$model->getId()) {
                    $session->addError($helper->__('This profile no longer exists.'));
                    $this->_redirect('*/*/new');
                    return;
                }
            }

            // save model
            try {
                $model->addData($data);
                $session->setFormData($data);
                $model->save();
                $session->setFormData(false);
                $session->addSuccess(
                    $helper->__('The profile has been saved.')
                );
            } catch (Mage_Core_Exception $e) {
                $session->addError($e->getMessage());
                $redirectBack = true;
            } catch (Exception $e) {
                $session->addError($helper->__('Unable to save the profile.'));
                $redirectBack = true;
                Mage::logException($e);
            }

            if ($redirectBack) {
                $this->_redirect('*/*/edit', array('id' => $model->getId()));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export file direct generation
     */
    public function generateAction()
    {
        $id = (int)Mage::app()->getRequest()->get('id');
        $session = Mage::getSingleton('adminhtml/session');
        $helper = Mage::helper('sellvana_sellvanaexport');
        if ($id) {
            try {
                /** @var Sellvana_SellvanaExport_Model_Profile $profile */
                $profile = Mage::getModel('sellvana_sellvanaexport/profile')->load($id);
                if ($profile->isRunning()) {
                    $message = $helper->__('The profile is already running.');
                    if ((int)Mage::app()->getStore()->getId() === 0) {
                        $forceUrl = Mage::helper('adminhtml')->getUrl('*/*/force', array('id' => $profile->getId()));
                        $message .= ' ' . $helper->__('You can <a href="%s">force</a> re-running it, if you are sure you need to.', $forceUrl);
                        Mage::throwException($message);
                    }
                }
                /** @var Sellvana_SellvanaExport_Model_Export $export */
                $export = Mage::getModel("sellvana_sellvanaexport/export", array('profile' => $profile));
                $export->export();
                $message = 'The export was finished successfully. ';
                $message .= 'You can now download <a href="%s">the export file</a>.';
                $url = $this->getUrl('*/*/getExportFile', array('id' => $id));
                $session->addSuccess($helper->__($message, $url));
            } catch (Exception $e) {
                $session->addError($e->getMessage());
                $this->_redirect('*/*/');
            }
        } else {
            $session->addError($helper->__('This profile no longer exists.'));
        }
        $this->_redirect('*/*/');
    }

    /**
     * Force profile run
     */
    public function forceAction()
    {
        $id = (int)Mage::app()->getRequest()->get('id');
        $session = Mage::getSingleton('adminhtml/session');
        if ($id) {
            try {
                $profile = Mage::getModel('sellvana_sellvanaexport/profile')->load($id);
                $profile->setData('run_status', Sellvana_SellvanaExport_Model_Profile::STATUS_NEW);
                $profile->save();
            } catch (Exception $e) {
                $session->addError($e->getMessage());
                $this->_redirect('*/*/');
            }
        }
        $this->generateAction();
    }

    /**
     * Download generated export file
     */
    public function getExportFileAction()
    {
        $id = (int)Mage::app()->getRequest()->get('id');
        $session = Mage::getSingleton('adminhtml/session');
        $helper = Mage::helper('sellvana_sellvanaexport');
        if ($id) {
            $profile = Mage::getModel('sellvana_sellvanaexport/profile')->load($id);
            $file = Mage::getBaseDir('var') . '/export/' . $profile->getFile();
            if (is_file($file)) {
                Mage::app()->getResponse()
                    ->setHttpResponseCode(200)
                    ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
                    ->setHeader('Pragma', 'public', true)
                    ->setHeader('Content-type', 'application/force-download')
                    ->setHeader('Content-Length', filesize($file))
                    ->setHeader('Content-Disposition', 'attachment' . '; filename=' . basename($file));
                $this->getResponse()->clearBody();
                $this->getResponse()->sendHeaders();
                readfile($file);
            } else {
                $message = $helper->__('The file for this profile does not exists. Re-run the profile to generate it.');
                $session->addError($message);
                $this->_redirect('*/*/');
            }
            return;
        } else {
            $session->addError($helper->__('This profile no longer exists.'));
        }
    }

    /**
     * Schedule export process to be executed on background (via cron)
     */
    public function scheduleAction()
    {
        $id = (int)Mage::app()->getRequest()->get('id');
        $session = Mage::getSingleton('adminhtml/session');
        $helper = Mage::helper('sellvana_sellvanaexport');
        if ($id) {
            $profile = Mage::getModel('sellvana_sellvanaexport/profile')->load($id);

            if (!$profile->getFile()) {
                $session->addError($helper->__('This profile no longer exists.'));
                $this->_redirect('*/*/');
            }

            if ($profile->isRunning()) {
                $session->addError($helper->__('The profile is already running.'));
                $this->_redirect('*/*/');
            }

            $cronExpression = date('i H d m w', time() + 5*60);
            try {
                $schedulePath = sprintf(self::CRON_STRING_PATH, $id);
                $modelPath = sprintf(self::CRON_MODEL_DEST_PATH, $id);
                Mage::getModel('core/config_data')
                    ->load($schedulePath, 'path')
                    ->setValue($cronExpression)
                    ->setPath($schedulePath)
                    ->save();
                Mage::getModel('core/config_data')
                    ->load($modelPath, 'path')
                    ->setValue((string)Mage::getConfig()->getNode(self::CRON_MODEL_SOURCE_PATH))
                    ->setPath($modelPath)
                    ->save();
                $profile->setData('run_status', Sellvana_SellvanaExport_Model_Profile::STATUS_SCHEDULED);
                $profile->save();
                $schedule = Mage::getModel('cron/schedule');
                $gmtDate = Mage::getSingleton('core/date')->gmtDate();
                $schedule->setData(array(
                    'job_code' => 'sellvanaexport_generate_profile_' . $id,
                    'status' => Mage_Cron_Model_Schedule::STATUS_PENDING,
                    'scheduled_at' => $gmtDate,
                    'created_at' => $gmtDate,
                ));
                $schedule->save();
                $message = 'The export is scheduled to be run on background. ';
                $message .= 'You will receive notification by email (%s) when it is finished.';
                $session->addSuccess($helper->__($message, Mage::getStoreConfig('trans_email/ident_general/email')));
            } catch (Exception $e) {
                $session->addError($e->getMessage());
            }
        } else {
            $session->addError($helper->__('This profile no longer exists.'));
        }

        $this->_redirect('*/*/');
    }
}