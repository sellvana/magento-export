<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Export
 */
class Sellvana_SellvanaExport_Model_Export extends Mage_Core_Model_Abstract
{
    const     STORE_UNIQUE_ID_KEY = '_store_unique_id';
    const     DEFAULT_FIELDS_KEY = '_default_fields';
    const     DEFAULT_MODEL_KEY = '_default_model';

    /** @var string Path to export file */
    protected $_filePath = 'var/export';
    /** @var  string Name of export file */
    protected $_fileFilename;

    /** @var  Sellvana_SellvanaExport_Model_Profile Config of export */
    protected $_profile;

    /** @var null|Varien_Io_File $_io */
    protected $_io;

    protected $_modelList = array(
        /** @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_Category */
        'Sellvana_Catalog_Category',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_MultiSite_Site */
        'Sellvana_MultiSite_Site',

        /** @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductBundle */
        'Sellvana_Catalog_ProductBundle',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable */
        'Sellvana_Catalog_ProductConfigurable',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_Field */
        'Sellvana_CatalogFields_Field',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_FieldOption */
        'Sellvana_CatalogFields_FieldOption',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVarfield */
        'Sellvana_CatalogFields_ProductVarfield',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVariant */
        'Sellvana_CatalogFields_ProductVariant',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVariantField */
        'Sellvana_CatalogFields_ProductVariantField',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_FcomCore_MediaLibrary */
        'Sellvana_FcomCore_MediaLibrary',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurableMedia */
        'Sellvana_Catalog_ProductConfigurableMedia',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimple */
        'Sellvana_Catalog_ProductSimple',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimpleMedia */
        'Sellvana_Catalog_ProductSimpleMedia',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVariantImage */
        'Sellvana_CatalogFields_ProductVariantImage',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_CategoryProduct */
        'Sellvana_Catalog_CategoryProduct',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductLink */
        'Sellvana_Catalog_ProductLink',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_InventorySku */
        'Sellvana_Catalog_InventorySku',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_Set */
        'Sellvana_CatalogFields_Set',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_SetField */
        'Sellvana_CatalogFields_SetField',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductFieldData */
        'Sellvana_CatalogFields_ProductFieldData',

        /** @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductFrontendFields */
        'Sellvana_Catalog_ProductFrontendFields',

        /** @see Sellvana_SellvanaExport_Model_Sellvana_Customer_Customer */
        'Sellvana_Customer_Customer',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_CustomerGroups_Group */
        'Sellvana_CustomerGroups_Group',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_Customer_Address */
        'Sellvana_Customer_Address',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_Customer_Customer */
        'Sellvana_Customer_Customer', //Is necessary for correctly work of foreign keys.
        /** @see Sellvana_SellvanaExport_Model_Sellvana_SalesTax_CustomerClass */

        /** @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductPrice */
        'Sellvana_Catalog_ProductPrice',

        /** @see Sellvana_SellvanaExport_Model_Sellvana_SalesTax_CustomerClass */
        'Sellvana_SalesTax_CustomerClass',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_SalesTax_ProductClass */
        'Sellvana_SalesTax_ProductClass',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_SalesTax_Zone */
        'Sellvana_SalesTax_Zone',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_SalesTax_Rule */
        'Sellvana_SalesTax_Rule',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_SalesTax_RuleCustomerClass */
        'Sellvana_SalesTax_RuleCustomerClass',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_SalesTax_RuleProductClass */
        'Sellvana_SalesTax_RuleProductClass',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_SalesTax_RuleZone */
        'Sellvana_SalesTax_RuleZone',

        /** @see Sellvana_SellvanaExport_Model_Sellvana_Sales_Cart */
        //'Sellvana_Sales_Cart',
        /** @see Sellvana_SellvanaExport_Model_Sellvana_Sales_CartItem */
        //'Sellvana_Sales_CartItem',

        /** @see Sellvana_SellvanaExport_Model_Sellvana_FcomCore_ExternalConfig */
        'Sellvana_FcomCore_ExternalConfig'
    );

    protected $_skipModelList = array(
//        //'Sellvana_Catalog_Category',
//
//        //'Sellvana_Catalog_ProductConfigurable',
//        'Sellvana_CatalogFields_Field',
//        'Sellvana_CatalogFields_FieldOption',
//        'Sellvana_CatalogFields_ProductVarfield',
//        'Sellvana_CatalogFields_ProductVariant',
//        'Sellvana_CatalogFields_ProductVariantField',
//        'Sellvana_FcomCore_MediaLibrary',
//        'Sellvana_Catalog_ProductConfigurableMedia',
//        'Sellvana_CatalogFields_ProductVariantImage',
//        //'Sellvana_Catalog_ProductSimple',
//        'Sellvana_Catalog_ProductSimpleMedia',
//        //'Sellvana_Catalog_CategoryProduct',
//        'Sellvana_Catalog_ProductLink',
//        'Sellvana_Catalog_InventorySku',
//        'Sellvana_Catalog_ProductPrice',
//        'Sellvana_CatalogFields_Set',
//        'Sellvana_CatalogFields_SetField',
//        'Sellvana_CatalogFields_ProductFieldData',
//
        'Sellvana_Catalog_ProductFrontendFields',
//
//        'Sellvana_Customer_Customer',
//        'Sellvana_CustomerGroups_Group',
//        'Sellvana_Customer_Address',
//
//        'Sellvana_SalesTax_CustomerClass',
//        'Sellvana_SalesTax_ProductClass',
//        'Sellvana_SalesTax_Zone',
//        'Sellvana_SalesTax_Rule',
//        'Sellvana_SalesTax_RuleCustomerClass',
//        'Sellvana_SalesTax_RuleProductClass',
//        'Sellvana_SalesTax_RuleZone',
//
//        'Sellvana_FcomCore_ExternalConfig'
    );

    /**
     * @inheritdoc
     */
    protected function _construct()
    {

        /** @var Sellvana_SellvanaExport_Model_Profile $profile */
        $profile = $this->getData('profile');
        if ($profile instanceof Sellvana_SellvanaExport_Model_Profile) {
            $this->_profile = $profile;
            $filename = $profile->getData('file');
            $this->setFileFilename($filename);
        } else {
            Mage::throwException('Profile not defined');
        }
    }

    /**
     * @return Varien_Io_File
     * @throws Mage_Core_Exception
     */
    public function getIO()
    {
        if (null === $this->_filePath) {
            Mage::throwException(Mage::helper('sellvana_sellvanaexport')->__('Please define file path'));
        }

        if (null === $this->_io) {
            $io = new Varien_Io_File();
            $realPath = $io->getCleanPath(Mage::getBaseDir() . '/' . $this->getFilePath());

            /**
             * Check path is allow
             */
            if (!$io->allowedPath($realPath, Mage::getBaseDir())) {
                Mage::throwException(Mage::helper('sellvana_sellvanaexport')->__('Please define correct path'));
            }

            if (!$io->fileExists($realPath)) {
                $io->mkdir($realPath);
            }

            /**
             * Check exists and writable path
             */
            if (!$io->fileExists($realPath, false)) {
                /** @var Mage_Core_Helper_Data $helper */
                $helper = Mage::helper('core');
                Mage::throwException(
                    Mage::helper('sellvana_sellvanaexport')->__(
                        'Please create the specified folder "%s" before saving the json.',
                        $helper->escapeHtml($this->getFilePath())
                    )
                );
            }

            if (!$io->isWriteable($realPath)) {
                Mage::throwException(
                    Mage::helper('sellvana_sellvanaexport')->__(
                        'Please make sure that "%s" is writable by web-server.',
                        $this->getFilePath()
                    )
                );
            }

            /**
             * Check allow filename
             */
            if (!preg_match('#^[a-zA-Z0-9_\.]+$#', $this->getFileFilename())) {
                Mage::throwException(
                    Mage::helper('sellvana_sellvanaexport')->__(
                        'Please use only letters (a-z or A-Z), numbers (0-9) or underscore (_) in the'
                        . ' filename. No spaces or other characters are allowed.'
                    )
                );
            }

            /*if (!preg_match('#\.json$#', $this->getFileFilename())) {
                $this->setFileFilename($this->getFileFilename() . '.json');
            }*/

            $io->setAllowCreateFolders(true);
            $io->open(array('path' => $realPath));

            $this->_io = $io;
        }
        return $this->_io;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->_filePath;
    }

    /**
     * @param $filePath
     */
    public function setFilePath($filePath)
    {
        $this->_filePath = $filePath;
    }

    /**
     * @return string
     */
    public function getFileFilename()
    {
        return $this->_fileFilename;
    }

    /**
     * @param string $filename
     */
    public function setFileFilename($filename)
    {
        $filename = trim($filename);
        if ($filename === '') {
            Mage::throwException('Filename can\'t be empty');
        }
        $this->_fileFilename = $filename;
    }

    /**
     * @return string
     */
    protected function _storeUID()
    {
        /** @var Mage_Core_Helper_Data $helper */
        $helper = Mage::helper('core');
        return $helper->getRandomString(32);
    }

    /**
     * @return Sellvana_SellvanaExport_Model_Export
     */
    public function export()
    {
        $this->_profile->setData('run_status', Sellvana_SellvanaExport_Model_Profile::STATUS_INPROGRESS);
        $this->_profile->save();

        $log = Mage::helper('sellvana_sellvanaexport/debug');
        $log->start();

        $io = $this->getIO();

        Mage::register('SellvanaExportStorage', Mage::getModel('sellvana_sellvanaexport/Sellvana_Storage'));

        $io->streamOpen($this->getFileFilename());

        $io->streamWrite("[\n");

        $fileHeadLine = array(self::STORE_UNIQUE_ID_KEY => $this->_storeUID());
        $io->streamWrite(json_encode($fileHeadLine) . "\n");

        gc_disable();
        foreach ($this->_modelList as $key => $modelName) {
            if (in_array($modelName, $this->_skipModelList)){
                continue;
            }
            /** @var Sellvana_SellvanaExport_Model_Sellvana_Abstract $model */
            $model = Mage::getModel(
                'sellvana_sellvanaexport/' . $modelName,
                array('io' => $io, 'profile' => $this->_profile)
            );
            $model->export();
            $this->_profile->setData('last_response', date('Y-m-d H:i:s'));
            $this->_profile->save();
            gc_collect_cycles();
        }
        gc_enable();
        $this->_profile->setData('last_success', date('Y-m-d H:i:s'));
        $this->_profile->setData('run_status', Sellvana_SellvanaExport_Model_Profile::STATUS_FINISHED);
        $this->_profile->save();

        $io->streamWrite("]");

        $io->streamClose();

        $log->end();
        $log->total(array_merge($this->_modelList, array('global')), 'console');

        return $this;
    }
}