<?php

/**
 * Class Sellvana_SellvanaExport_Model_Profile
 *
 * @method string getFile()
 * @method string getEntities()
 * @method datetime getLastSuccess()
 * @method string setEntities() setEntities($data)
 * @method string setFile() setFile($data)
 */
class Sellvana_SellvanaExport_Model_Profile extends Mage_Core_Model_Abstract
{
    const STATUS_NEW        = 'new';
    const STATUS_SCHEDULED  = 'scheduled';
    const STATUS_INPROGRESS = 'in_progress';
    const STATUS_FINISHED   = 'done';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('sellvana_sellvanaexport/profile');
    }

    /**
     * @inheritdoc
     */
    protected function _beforeSave()
    {
        $fileName = $this->getFile();
        if (strpos($fileName, '.') === false) {
            $fileName .= '.json';
            $this->setFile($fileName);
        }
        $entities = $this->getEntities();

        if (is_array($entities)) {
            $this->setEntities(json_encode($entities));
        }

        return parent::_beforeSave();
    }

    protected function _decodeEntities()
    {
        $entities = json_decode($this->getEntities());
        $entities = is_array($entities) ? $entities : array();

        $this->setEntities($entities);
    }

    /**
     * @inheritdoc
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();

        $this->_decodeEntities();

        return $this;
    }

    protected function _afterSave()
    {
        $this->_decodeEntities();

        return $this;
    }


    /**
     * @return bool
     */
    public function isRunning()
    {
        return $this->getData('run_status') == self::STATUS_INPROGRESS;
    }

}