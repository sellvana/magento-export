<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Cron
 */
class Sellvana_SellvanaExport_Model_Cron
{
    /**
     * Execute scheduled export
     *
     * @param Varien_Object $observer
     * @throws Mage_Core_Exception
     */
    public function generate($observer)
    {
        $jobCode = explode('_', $observer->getData('job_code'));
        $id = (int)array_pop($jobCode);
        $notifyData = array(
            "profileId" => $id,
        );
        if ($id) {
            try {
                $profile = Mage::getModel('sellvana_sellvanaexport/profile')->load($id);
                $export = Mage::getModel("sellvana_sellvanaexport/export", array('profile' => $profile));
                $export->export();
                $profile->setData('last_success', date('Y-m-d H:i:s'));
                $profile->save();
                $url = Mage::getUrl('adminhtml/sellvanaexport_profile/getExportFile', array('id' => $id));
                $notifyData["result"] = "Success\nYou can download the file from " . $url;
            } catch (Exception $e) {
                $notifyData["result"] = "Failure\n" . $e->getMessage();
            }

            /** @var Mage_Core_Model_Email_Template $emailTemplate */
            $emailTemplate = Mage::getModel('core/email_template')->setDesignConfig(array('area' => 'backend'));

            $to = Mage::getStoreConfig('trans_email/ident_general/email');
            $emailTemplate->sendTransactional(
                'sellvana_sellvanaexport_notification',
                'general',
                trim($to),
                trim($to),
                $notifyData
            );
        }
    }
}