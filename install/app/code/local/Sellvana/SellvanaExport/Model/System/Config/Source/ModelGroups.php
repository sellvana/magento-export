<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups
 */
class Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups
{
    const MODEL_GROUP_WEBSITES = 'websites';
    const MODEL_GROUP_ATTRIBUTE = 'attributes';
    const MODEL_GROUP_PRODUCT = 'products';
    const MODEL_GROUP_CATEGORY = 'categories';
    const MODEL_GROUP_CUSTOMER = 'customers';
    const MODEL_GROUP_CUSTOMER_GROUP = 'customer_groups';
    const MODEL_GROUP_TAX = 'taxes';
    const MODEL_GROUP_ORDER = 'orders';

    /**
     * @return array
     */
    public function toArray()
    {
        $helper = Mage::helper('sellvana_sellvanaexport');

        return array(
            self::MODEL_GROUP_WEBSITES => $helper->__('Websites'),
            self::MODEL_GROUP_ATTRIBUTE => $helper->__('Attributes'),
            self::MODEL_GROUP_PRODUCT => $helper->__('Products'),
            self::MODEL_GROUP_CATEGORY => $helper->__('Categories'),
            self::MODEL_GROUP_CUSTOMER => $helper->__('Customers'),
            self::MODEL_GROUP_CUSTOMER_GROUP => $helper->__('Customer Groups'),
            self::MODEL_GROUP_TAX => $helper->__('Taxes'),
            self::MODEL_GROUP_ORDER => $helper->__('Orders'),
        );
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $groups = array();
        foreach ($this->toArray() as $value => $label) {
            $groups[] = array(
                'label' => $label,
                'value' => $value
            );
        }

        return $groups;
    }
}