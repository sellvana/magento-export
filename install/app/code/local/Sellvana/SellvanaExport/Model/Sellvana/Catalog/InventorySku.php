<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_Catalog_InventorySku
 */
class Sellvana_SellvanaExport_Model_Sellvana_Catalog_InventorySku
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName        = 'Sellvana_Catalog_Model_InventorySku';
    protected $_magentoModelName = 'cataloginventory/stock';
    protected $_modelGroups      = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT
    );
    protected $_uniqueKey        = 'inventory_sku';

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        "id"                => "PK",
        "inventory_sku"     => "sku",
        "title"             => "title",
        //"description"       => "",
        //"is_salable"        => "",
        //"bin_id"            => "",//ask: Learn for what this field
        //"unit_cost"         => "",
        //"net_weight"        => "",
        //"shipping_weight"   => "",
        //"shipping_size"     => "",
        "pack_separate"     => "pack_separate",
        "qty_in_stock"      => "qty",//ask: What to do if the remains is in decimal
        //"qty_warn_customer" => "",//ask: When this field will be used in sellvana return to its implementation
        //"qty_notify_admin"  => "",
        "qty_cart_min"      => "min_sale_qty",
        "qty_cart_inc"      => "qty_increments",
        "qty_buffer"        => "qty_buffer",//ask: Learn for what this field
        "qty_reserved"      => "qty_reserved",//The Magento don't have this functionality
        //"create_at"         => "",
        //"update_at"         => "",
        //"data_serialized"   => "",
        "allow_backorder"   => "backorders",
        "manage_inventory"  => "manage_stock",
        //"hs_tariff_number"  => "",//ask: Learn for what this field
        //"origin_country"    => "",//ask: Learn for what this field
    );

    protected $_globalSettings = array();

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'sku'            => 'ruleCheckSkuRewrite',
                'title'          => array('ruleSetName', 'ruleNotNull'),
                'pack_separate'  => 'ruleZero',
                'qty'            => array('ruleInt', 'ruleString'),
                'min_sale_qty'   => array('ruleInt', 'ruleMinSaleQty'),
                'qty_increments' => array('ruleInt', 'ruleQtyIncrements'),
                'qty_buffer'     => 'ruleZero',
                'qty_reserved'   => 'ruleZero',
                'backorders'     => 'ruleBackorders',
                'manage_stock'   => 'ruleManageStock'
            )
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection->addAttributeToFilter('type_id', array('in' => array('simple', 'configurable')));
    }

    /**
     * @inheritdoc
     */
    protected function _export()
    {
        /** @var Mage_CatalogInventory_Model_Stock $itemStockModel */
        $itemStockModel = Mage::getModel($this->_magentoModelName);

        $counter = 0;
        $offsetSize = $this->_selectOffsetSize;
        while (true) {
            /** @var Mage_CatalogInventory_Model_Resource_Stock_Item_Collection $collection */
            $collection = $itemStockModel->getItemCollection();

            $select = $collection->getSelect();
            $select->columns('cp_table.sku');
            $select->limit($offsetSize, $offsetSize * $counter++);

            if ($collection->count() == 0 && $counter ==0) {
                return null;
            }
            if ($collection->count() == 0) {
                break;
            }

            foreach ($collection as $model) {
                if ($this->skip($model)) {
                    continue;
                }

                $modelData = $this->_prepareData($model);

                $this->writeToFile($modelData);
            }
        }

        return $this;
    }

    /**
     * Set name.
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleSetName(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, 'N/A');
        return true;
    }

    /**
     * Set 'Back Orders' given the global settings
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleBackorders(Varien_Object $model, $attribute)
    {
        $useGlobal = $model->getData('use_config_backorders');
        if ($useGlobal) {
            if (array_key_exists('backorders', $this->_globalSettings)) {
                $value = $this->_globalSettings['backorders'];
            } else {
                $value = (Mage::getStoreConfig('cataloginventory/item_options/backorders') <= 1 ? '1' : '0');
                $this->_globalSettings['backorders'] = $value;
            }
        } else {
            $value = (($model->getData($attribute) <= 1) ? '1' : '0');
        }

        $model->setData($attribute, $value);

        return true;
    }

    /**
     * Set 'Minimum Qty Allowed in Shopping Cart' given the global settings
     *
     * Setting depends on the user group, but Selvana didn't have this functionality.
     * So we ignore all groups, except 'ALL GROUPS'
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleMinSaleQty(Varien_Object $model, $attribute)
    {
        $useGlobal = $model->getData('use_config_min_qty');
        if ($useGlobal) {
            if (array_key_exists('min_sale_qty', $this->_globalSettings)) {
                $value = $this->_globalSettings['min_sale_qty'];
            } else {
                $coreConfig = (Mage::getStoreConfig('cataloginventory/item_options/min_sale_qty'));
                $value = @unserialize($coreConfig); //Get groups.
                if (($value === false)) {//ALL GROUPS
                    $value = $coreConfig;
                } elseif (is_array($value) && empty($value)) {//Assigned nothing
                    $value = '0';
                } elseif (is_array($value) && array_key_exists(32000, $value)) {//ALL GROUPS
                    $value = (int)$value[32000];
                } else {//Ignore groups
                    $value = '0';
                }

                $this->_globalSettings['min_sale_qty'] = $value;
            }

            $model->setData($attribute, (string)$value);
        }
        return true;
    }

    /**
     * Set 'Qty in Cart Increment' given the global settings
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleQtyIncrements(Varien_Object $model, $attribute)
    {
        $useGlobal = $model->getData('use_config_qty_increments');
        if ($useGlobal) {
            if (array_key_exists('qty_increments', $this->_globalSettings)) {
                $value = $this->_globalSettings['qty_increments'];
            } else {
                if (Mage::getStoreConfig('cataloginventory/item_options/enable_qty_increments')) {
                    $value = Mage::getStoreConfig('cataloginventory/item_options/qty_increments');
                } else {
                    $value = '0';
                }

                $this->_globalSettings['qty_increments'] = $value;
            }
            $model->setData($attribute, (string)$value);

        } else {
            if (!$model->getData('enable_qty_increments')) {
                $model->setData($attribute, '0');
            } else {
                $model->setData($attribute, (string)$model->getData($attribute));
            }
        }

        return true;
    }

    /**
     * Set 'Manage Inventory' given the global settings
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleManageStock(Varien_Object $model, $attribute)
    {
        $useGlobal = $model->getData('use_config_manage_stock');
        if ($useGlobal) {
            if (array_key_exists('manage_stock', $this->_globalSettings)) {
                $value = $this->_globalSettings['manage_stock'];
            } else {
                $configPath = 'cataloginventory/item_options/manage_stock';
                $this->_globalSettings['manage_stock'] = Mage::getStoreConfig($configPath);
                $value = $this->_globalSettings['manage_stock'];
            }

            $model->setData($attribute, (string)$value);
        }
        return true;
    }

    /**
     * Set sku to rewrite if exist.
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleCheckSkuRewrite(Varien_Object $model, $attribute)
    {
        $id = $model->getData('product_id');

        /** @see Sellvana_SellvanaExport_Model_Sellvana_Abstract::ruleUnique() */
        $sku = $this->_storage->getProductData('sku', $id);
        if (null !== $sku) {
            $this->setData($attribute, $sku);
        }
        return true;
    }
}