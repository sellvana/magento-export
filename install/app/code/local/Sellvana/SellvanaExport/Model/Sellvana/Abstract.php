<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_Abstract
 */
abstract class Sellvana_SellvanaExport_Model_Sellvana_Abstract extends Mage_Core_Model_Abstract
{
    const     DEFAULT_FIELDS_KEY = '_default_fields';
    const     DEFAULT_MODEL_KEY = '_default_model';

    protected $_selfName;
    protected $_sellvanaModelName;
    protected $_magentoModelName;

    /** @var array */
    protected $_modelGroups = array();
    /** @var  Sellvana_SellvanaExport_Model_Profile Config of export */
    protected $_profile;
    protected $_defaultFields = array();
    protected $_uniqueKey;
    protected $_uniqueKeys = array();

    /** @var  array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array();

    /** @var  null|Varien_Io_File */
    protected $_io;
    /** @var  null|Sellvana_SellvanaExport_Model_Sellvana_Storage */
    protected $_storage;
    protected $_rules = array();

    /** @var  Sellvana_SellvanaExport_Helper_Debug */
    public $log;

    protected $_lastAutoIncrementId;

    protected $_selectOffsetSize = 1000;

    protected static $_requiredTransliterationRules = array(
        'Any-Latin',
        'Latin-ASCII'
    );

    protected $_availableTransliterationRules = '';

    /**
     * Get set of rules for this model.
     *
     * Validate section:
     * MagentoField|MagentoAttribute => callback
     *
     * Skip section:
     * string|array of callback (The rule for checking if model processing is necessary)
     * @return array
     */
    public function rules()
    {
        return array(
            'validate' => array(
                'PK' => 'rulePrimaryKey'
            ),
            'skip' => array()
        );
    }

    /**
     * Init model
     */
    public function _construct()
    {
        $this->log = Mage::helper('sellvana_sellvanaexport/debug');
        $this->_defaultFields = array_keys($this->_defaultFieldsMap);
        $this->_rules = $this->rules();
        $this->_storage = Mage::registry('SellvanaExportStorage');

        if (null === $this->_selfName) {
            $class = get_class($this);
            $this->_selfName = str_replace('Sellvana_SellvanaExport_Model_', '', $class);
        }

        /** @var  Varien_Io_File $io */
        $io = $this->getData('io');
        if ($io instanceof Varien_Io_File) {
            $this->_io = $io;
        } else {
            Mage::throwException('IO is not defined');
        }

        /** @var  Sellvana_SellvanaExport_Model_Profile $profile */
        $profile = $this->getData('profile');
        if ($profile instanceof Sellvana_SellvanaExport_Model_Profile) {
            $this->_profile = $profile;
        } else {
            Mage::throwException('Profile not defined');
        }

        if (function_exists('transliterator_list_ids')) {
            $systemTransliterationRules = transliterator_list_ids();
            $rules = array_intersect(static::$_requiredTransliterationRules, $systemTransliterationRules);
            $rules[] = 'Lower()';
            $this->_availableTransliterationRules = implode('; ', $rules);
        }
    }

    /**
     * @return array
     */
    protected function _generateHeading()
    {
        return array(
            static::DEFAULT_MODEL_KEY => $this->_sellvanaModelName,
            static::DEFAULT_FIELDS_KEY => $this->_defaultFields
        );
    }

    /**
     * @param string|array $data
     * @param bool|true $encoded
     * @param string $lineSeparator
     * @return bool
     * @throws Mage_Core_Exception
     */
    protected function writeToFile($data, $encoded = true, $lineSeparator = "\n")
    {
        $io = $this->_io;
        if (!($io instanceof Varien_Io_File)) {
            Mage::throwException(Mage::helper('sellvana_sellvanaexport')->__('Please define file IO'));
        }
        if (is_array($data) && $encoded) {
            foreach ($data as $line) {
                $io->streamWrite("," . $line . $lineSeparator);
            }
        }
        if (is_string($data) && $encoded) {
            $io->streamWrite("," . $data . $lineSeparator);
        }
        if ((is_array($data) || is_string($data)) && !$encoded) {
            $io->streamWrite("," . json_encode($data) . $lineSeparator);
        }
        return true;
    }

    /**
     * @param Varien_Object $model
     * @param null|array $map
     * @return array
     */
    protected function _prepareData(Varien_Object $model, $map = null)
    {
        //$this->log->start(get_class($this) . '-' . $model->getId());
        if (null === $map) {
            $map = $this->_defaultFieldsMap;
        }

        $result = array();
        foreach ($map as $field => $attribute) {
            $result[$field] = $this->getValidatedData($attribute, $model);
        }

        //$this->log->end(get_class($this) . '-' . $model->getId());
        return json_encode(array_values($result));
    }

    /**
     * @param Varien_Data_Collection_Db $collection
     */
    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        $this->_addAttributesToCollection($collection);
    }

    /**
     * @param Varien_Data_Collection_Db $collection
     * @return void
     */
    protected function _addAttributesToCollection(Varien_Data_Collection_Db $collection)
    {
        if ($collection instanceof Mage_Eav_Model_Entity_Collection_Abstract) {
            $attributes = $this->_defaultFieldsMap;
            unset($attributes['id']);
            $attributes = array_values($attributes);
            $collection->addAttributeToSelect($attributes);
        }
    }

    /**
     * @return bool
     */
    protected function _canExport()
    {
        $profileGroups = $this->_profile->getData('entities');
        $return = true;
        foreach ($this->_modelGroups as $mainGroup) {
            if (!in_array($mainGroup, $profileGroups)) {
                $return = false;
            }
        }

        return $return;
    }

    /**
     * @return $this
     */
    public function export()
    {
        if (!$this->_canExport()) {
            return $this;
        }
        $this->log->start($this->_selfName, true);
        $this->_lastAutoIncrementId = (int)$this->_storage->getData(
            $this->_sellvanaModelName,
            'autoincrements',
            'core'
        );
        if (!$this->_lastAutoIncrementId) {
            $this->_lastAutoIncrementId = 1;
        }

        $this->writeToFile(json_encode($this->_generateHeading()));
        $this->_export();
        $this->log->end($this->_selfName, true);

        $this->_storage->setData(
            $this->_sellvanaModelName,
            $this->_lastAutoIncrementId,
            'autoincrements',
            'core'
        );

        return $this;
    }

    /**
     * @return $this|null
     */
    protected function _export()
    {
        /** @var  Sellvana_SellvanaExport_Model_Sellvana_Abstract $model */
        $model = Mage::getModel($this->_magentoModelName);

        /** @var Mage_Core_Model_Resource_Db_Collection_Abstract $collection */

        $counter = 0;
        $offsetSize = $this->_selectOffsetSize;

        $collection = $model->getCollection();
        $this->_prepareCollection($collection);
        $select = $collection->getSelect();
        $select->limit($offsetSize, $offsetSize * $counter++);

        if ($collection->count() == 0) {
            return null;
        }

        while ($collection->count() > 0) {
            $start = microtime(true);

            foreach ($collection as $model) {
                if ($this->skip($model)) {
                    continue;
                }

                $modelData = $this->_prepareData($model);

                $this->writeToFile($modelData);
            }

            $collection->clear();
            $select->limit($offsetSize, $offsetSize * $counter++);

            $end = microtime(true) - $start;
            //echo sprintf("%.2F", $end) . "s\n";
        }
        $collection = null;
        unset($collection);

        return $this;
    }

    /**
     * Get validated attribute data.
     *
     * @param string $attribute
     * @param Varien_Object $model
     * @return mixed
     */
    public function getValidatedData($attribute, Varien_Object $model)
    {
        if (array_key_exists('validate', $this->_rules) && array_key_exists($attribute, $this->_rules['validate'])) {
            $rules = $this->_rules['validate'];

            if (!is_array($rules[$attribute])) {
                $rules[$attribute] = array($rules[$attribute]);
            }
            foreach ($rules[$attribute] as $rule) {
                if (method_exists($this, $rule)) {
                    if (!call_user_func_array(array($this, $rule), array($model, $attribute))) {
                        Mage::throwException(Mage::helper('sellvana_sellvanaexport')->__(
                            'An error occurred during validation. Validator: '
                            . get_class($this) . "::" . $rule
                        ));
                    }
                }
            }
        }
        return $model->getData($attribute);
    }

    /**
     * @param Varien_Object|array $model
     * @return bool|mixed
     */

    /**
     * @param Varien_Object $model
     * @return bool
     */
    public function skip(Varien_Object $model)
    {
        if (array_key_exists('skip', $this->_rules)) {
            $rules = $this->_rules['skip'];

            if (!is_array($rules)) {
                $rules = array($rules);
            }
            foreach ($rules as $rule) {
                if (!method_exists($this, $rule)) {
                    continue;
                }
                if (call_user_func_array(array($this, $rule), array($model))) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param Varien_Object $model
     * @return bool
     */
    public function skipUnique(Varien_Object $model)
    {
        $keyAttributes = array();
        foreach ((array)$this->_uniqueKey as $key) {
            $keyAttributes[$key] = $model->getData($this->_defaultFieldsMap[$key]);
        }
        $key = implode('/', $keyAttributes);
        if (array_key_exists($key, $this->_uniqueKeys)) {
            return true;
        } else {
            $this->_uniqueKeys[$key] = true;
        }

        return false;
    }

    /**
     * Set model PK to attribute value.
     *
     * @param Varien_Object|Mage_Catalog_Model_Abstract $model
     * @param $attribute
     * @return bool
     */
    public function rulePrimaryKey(Varien_Object $model, $attribute)
    {
        $key = $model->getData('PK');
        if ($model instanceof Mage_Core_Model_Abstract) {
            $key = $model->getId();
        }

        $model->setData($attribute, $key);
        return true;
    }

    /**
     * Set unique value of attribute.
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleUnique(Varien_Object $model, $attribute)
    {
        //TODO: need refactoring. Maybe before export make query and get all duplicates.
        $value = $model->getData($attribute);
        $tmpValue = trim($value);
        $i = 0;
        $saveToStorage = false;
        if (!array_key_exists($attribute, $this->_uniqueKeys)) {
            $this->_uniqueKeys[$attribute] = array();
        }

        do {
            if ($i > 0) {
                $tmpValue = $value . '-' . $i;
                $saveToStorage = true;
            }
            $i++;
            $prevCount = count($this->_uniqueKeys[$attribute]);
            $processedValue = iconv("UTF-8", "ISO-8859-1//TRANSLIT", mb_strtolower($tmpValue));
            $this->_uniqueKeys[$attribute][$processedValue] = true;
        } while (count($this->_uniqueKeys[$attribute]) == $prevCount);

        if ($saveToStorage) {
            if ($this instanceof Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimple) {
                $this->_storage->setProductData($attribute, $tmpValue, $model->getId());
            }
        }

        $this->_uniqueKeys[$attribute][$tmpValue] = true;
        $model->setData($attribute, $tmpValue);
        return true;
    }

    /**
     * Generate attribute value from name if it's null.
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleNotNull(Varien_Object $model, $attribute)
    {
        $value = $model->getData($attribute);
        if (null === $value) {
            $value = $model->getData('name');
            $model->setData($attribute, $value);
        }
        return true;
    }

    /**
     * Set correct data for url_key or url_path
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleUrlValidate(Varien_Object $model, $attribute)
    {
        $value = $model->getData($attribute);
        $value = strtolower($value);
        //TODO: Can be problem with cyrillic, maybe need use mb_*
        $value = str_replace(' ', '-', $value);
        $value = preg_replace('![^\w\d\_\-\.\/]*!', '', $value);
        $model->setData($attribute, $value);

        return true;
    }

    /**
     * Set reverted attribute value, only for bool data;
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleRevert(Varien_Object $model, $attribute)
    {
        $value = (int)$model->getData($attribute);
        if ($value == 1) {
            $model->setData($attribute, '0');
        } else {
            $model->setData($attribute, '1');
        }
        return true;
    }

    /**
     * Set int type value.
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleInt(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, (int)$model->getData($attribute));
        return true;
    }

    /**
     * Set zero to value.
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleZero(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, '0');
        return true;
    }

    /**
     * Set string type to value.
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleString(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, (string)$model->getData($attribute));
        return true;
    }


    /**
     * Set '1' to value, if it's zero.
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleNotZero(Varien_Object $model, $attribute)
    {
        if ($model->getData($attribute) === '0') {
            $model->setData($attribute, '1');
        }
        return true;
    }

    /**
     * Set null to value, if it's zero.
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleZeroToNull(Varien_Object $model, $attribute)
    {
        if ($model->getData($attribute) === '0') {
            $model->setData($attribute, null);
        }
        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleMysqlDate(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, date('Y-m-d H:i:s', strtotime($model->getData($attribute))));

        return true;
    }

    /**
     * Set null to value
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleSetNull(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, null);

        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleVirtualAutoIncrement(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, $this->_lastAutoIncrementId++);

        return true;
    }

    public function slugify($string) {
        $string = transliterator_transliterate(
            "Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; [:Punctuation:] Remove; Lower();",
            $string
        );
        $string = preg_replace('/[-\s]+/', '-', $string);
        return trim($string, '-');
    }
}