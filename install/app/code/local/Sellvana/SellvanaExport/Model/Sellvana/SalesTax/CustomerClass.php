<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_SalesTax_CustomerClass
 */
class Sellvana_SellvanaExport_Model_Sellvana_SalesTax_CustomerClass
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_SalesTax_Model_CustomerClass';
    protected $_magentoModelName  = 'tax/class';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_CUSTOMER,
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_TAX
    );
    protected $_uniqueKey        = 'title';

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'        => 'PK',
        'title'     => 'class_name',
        //'notes'     => '',
        //'create_at' => '',
        //'update_at' => ''
    );

    protected $_salesClassType = Mage_Tax_Model_Class::TAX_CLASS_TYPE_CUSTOMER;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'created_at' => 'ruleMysqlDate'
            ),
            'skip' => 'skipClass'
        ));
    }

    /**
     * @param Varien_Object $model
     * @return bool
     */
    public function skipClass(Varien_Object $model)
    {
        return (bool)($model->getData('class_type') != $this->_salesClassType);
    }
}