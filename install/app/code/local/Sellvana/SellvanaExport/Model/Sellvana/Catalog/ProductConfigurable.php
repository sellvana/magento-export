<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable
 */
class Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_Catalog_Model_Product';
    protected $_magentoModelName  = 'catalog/product';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT,
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_ATTRIBUTE
    );
    protected $_uniqueKey        = 'product_sku';

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'                => 'PK',
        'product_sku'       => 'sku', //Sellvana requires a unique and not null value
        'product_name'      => 'name',
        'short_description' => 'short_description',
        'description'       => 'description',
        'url_key'           => 'url_key', //Sellvana requires a unique and not null value
        //'net_weight'        => null,
        //'ship_weight'       => null,
        'is_hidden'         => 'status', //ask: status or visibility
        //'notes'             => null,
        //'uom'               => 'EACH', //ask: find out what that is
        //'thumb_url'         => null,
        //'images_data'       => null,
        //'data_serialized'   => null,
        //'is_featured'       => null,
        //'is_popular'        => null,
        //'position'          => null,
        'inventory_sku'     => 'inventory_sku',
        //'manage_inventory'  => '0',//ask: find out what that is
        //'avg_rating'        => null,
        //'num_reviews'       => null
        'custom_details_view' => 'sellvana_custom_details_view',
        '_custom_data'        => 'sellvana_custom_data',
    );

    /** @var array ids of products included in the export */
    protected $_processed = array();

    /** @var array */
    protected $_storeLocales = array();

    /** @var array  */
    protected static $_translatableFields = array(
        'product_name_lang_fields' => 'name',
        'short_description_lang_fields' => 'short_description',
        'description_lang_fields' => 'description'
    );

    /** @var array  */
    protected static $_translatableFieldTypes = array(
        'product_name_lang_fields' => 'text',
        'short_description_lang_fields' => 'textarea',
        'description_lang_fields' => 'wysiwyg'
    );

    /** @var array */
    protected $_textFieldTranslations = array();

    /** @var array  */
    protected $_attributeCodeById = array();

    /**
     * @inheritdoc
     */
    public function _construct()
    {
        parent::_construct();
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        //ruleUnique is to slow for big data.
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'sku' => array('ruleNotNullSKU', 'ruleUnique'),
                /** @see ruleNotNullName() */
                'name' => 'ruleNotNullName',
                'url_key' => array('ruleNotNull', 'ruleUrlValidate', 'ruleUnique'),
                'status' => 'ruleRevert',
                'inventory_sku' => 'ruleInventorySku',
                /** @see ruleLoadTranslations() */
                'sellvana_custom_data' => 'ruleLoadTranslations',
            ),
            'skip' => array('skipNotConfigurable')
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection->addAttributeToFilter('type_id', array('in' => array( 'configurable')));
        $productIds = $collection->getAllIds();

        /** @var Mage_Eav_Model_Config $eavConfig */
        $eavConfig = Mage::getModel('eav/config');
        $entityTypeId = $eavConfig->getEntityType(Mage_Catalog_Model_Product::ENTITY)->getEntityTypeId();
        foreach (self::$_translatableFields as $code) {
            $attribute = $eavConfig->getAttribute(Mage_Catalog_Model_Product::ENTITY, $code);
            $backendTable = $attribute->getBackendTable();
            $this->_attributeCodeById[$attribute->getId()] = $code;
            /** @var Varien_Db_Adapter_Interface $connection */
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $select = $connection->select();
            $select->from(array('main_table' => $backendTable));
            $select->where('entity_type_id = ?', $entityTypeId);
            $select->where('attribute_id = ?', $attribute->getId());
            $select->where('store_id > 0');
            $select->where('entity_id IN (?)', $productIds);
            foreach ($connection->fetchAll($select) as $value) {
                $productId = $value['entity_id'];
                $attributeId = $value['attribute_id'];
                $storeId = $value['store_id'];
                if (!array_key_exists($storeId, $this->_storeLocales)) {
                    $this->_storeLocales[$storeId] = Mage::getStoreConfig('general/locale/code', $storeId);
                }

                if (!array_key_exists($productId, $this->_textFieldTranslations)) {
                    $this->_textFieldTranslations[$productId] = array();
                }
                $code = $this->_attributeCodeById[$attributeId];
                if (!array_key_exists($code, $this->_textFieldTranslations[$productId])) {
                    $this->_textFieldTranslations[$productId][$code] = array();
                }
                $this->_textFieldTranslations[$productId][$code][$storeId] = $value['value'];
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function _export()
    {
        $return = parent::_export();
        $processedProducts = (array)$this->_storage->getProductData('processed', 'products');
        $this->_storage->setProductData('processed', $this->_processed + $processedProducts, 'products');
        $this->_storage->setProductData('unique_keys', $this->_uniqueKeys, 'products');
        return $return;
    }

    /**
     * Check if is necessary to skip product
     * If product doesn't have to be skipped, add it id to @see $_processed
     *
     * @param Mage_Catalog_Model_Product $model
     * @return bool
     */
    public function skipNotConfigurable(Mage_Catalog_Model_Product $model)
    {
        $return = ($model->getTypeId() !== 'configurable');
        if (!$return) {
            $this->_processed[$model->getId()] = 'configurable';
        }

        return $return;
    }

    /**
     * Set unique SKU if it's null
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleNotNullSKU(Varien_Object $model, $attribute)
    {
        $value = $model->getData($attribute);
        if (null === $value) {
            /** @var Mage_Core_Helper_Data $helper */
            $helper = Mage::helper('core');
            $value = $helper->getRandomString(12);
            $model->setData($attribute, $value);
        }
        return true;
    }

    /**
     * Generate attribute value from name if it's null.
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    /*public function ruleNotNull(Varien_Object $model, $attribute)
    {
        $value = $model->getData($attribute);
        if (null === $value) {
            echo 1;
            $value = $model->getData('sku') . "_" . $model->getData('name');
            $model->setData($attribute, $value);
        }
        return true;
    }*/

    /**
     * Set 'inventory_sku' from 'product_sku'
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleInventorySku(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, $model->getData('sku'));
        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleNotNullName(Varien_Object $model, $attribute)
    {
        $value = $model->getData($attribute);
        if (null === $value) {
            $value = $model->getData('sku');
            $model->setData($attribute, $value);
        }
        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleLoadTranslations(Varien_Object $model, $attribute)
    {
        $customData = array();
        foreach (self::$_translatableFields as $sellvanaField => $magentoField) {
            if (isset($this->_textFieldTranslations[$model->getId()][$magentoField])) {
                $customData[$sellvanaField] = '';
                $tmpArray = array();
                foreach ($this->_textFieldTranslations[$model->getId()][$magentoField] as $storeId => $storeValue) {
                    $tmpArray[] = array(
                        'value' => $storeValue,
                        'lang_code' => $this->_storeLocales[$storeId],
                        'input_type' => self::$_translatableFieldTypes[$sellvanaField]
                    );
                }
                $customData[$sellvanaField] = json_encode($tmpArray);
            }
        }

        $model->setData($attribute, $customData);
        $model->setData('sellvana_custom_details_view', $customData);

        return true;
    }
}