<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_Set
 */
class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_Set extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName        = 'Sellvana_CatalogFields_Model_Set';
    protected $_magentoModelName = '';
    protected $_modelGroups      = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_ATTRIBUTE
    );
    protected $_uniqueKey        = array(
        'set_type',
        'set_code'
    );

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'       => 'PK',//"1"
        'set_type' => 'set_type',//"product"
        'set_code' => 'set_code',//"default"
        'set_name' => 'set_name' //"Default"
    );

    /**
     * @inheritdoc
     */
    protected function _export()
    {
        /** @var $dbi Varien_Db_Adapter_Interface */
        $dbi = Mage::getSingleton('core/resource')->getConnection('core_read');

        $entityType = Mage::getModel('catalog/product')->getResource()->getTypeId();

        /** @var Mage_Eav_Model_Resource_Entity_Attribute_Set_Collection $collection */
        $collection = Mage::getResourceModel('eav/entity_attribute_set_collection');
        $collection->setEntityTypeFilter($entityType);

        $resource = Mage::getSingleton('core/resource');

        $select = $collection->getSelect();
        $select
            ->joinInner(
                array('eav_sets' => $resource->getTableName('eav/attribute_group')),
                'main_table.attribute_set_id = eav_sets.attribute_set_id',
                array('*')
            )
            //->group('eav_sets.attribute_group_name')
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns('eav_sets.*');

        /** @var Sellvana_SellvanaExport_Helper_Array $arrayHelper */
        $arrayHelper = Mage::helper('sellvana_sellvanaexport/array');

        $rows = $dbi->fetchAll($select);

        $groupIds = $arrayHelper->map($rows, 'attribute_group_id', 'attribute_group_name', 'attribute_group_name');
        $tmpRows = array();
        foreach ($groupIds as $key => $row) {
            $tmpRows[$key] = implode(', ', array_keys($row));
        }
        $groupIds = $tmpRows;

        unset($tmpRows);

        $rows = $arrayHelper->map($rows, 'attribute_group_name', 'attribute_group_id');

        $autoIncrement = 1;
        foreach ($rows as $groupName => $key) {
            $result = array();
            foreach ($this->_defaultFieldsMap as $field => $attribute) {
                switch ($attribute){
                    case 'PK':
                        $value = (string)$autoIncrement++;
                        break;
                    case 'set_type':
                        $value = 'product';
                        break;
                    case 'set_code':
                        $value = strtolower($groupName);
                        $value = str_replace(' ', '-', $value);
                        $value = preg_replace('![^\w\d\_\-\.\/]*!', '', $value);
                        break;
                    case 'set_name':
                        $value = $groupName;
                        break;
                    default:
                        continue 2;
                }
                $result[$field] = $value;
            }

            if (array_key_exists($groupName, $groupIds)) {
                $groupIds[$result['id']] = $groupIds[$groupName];
                unset($groupIds[$groupName]);
            }

            $modelData = json_encode(array_values($result));

            $this->writeToFile($modelData);
        }

        $tmp = array();
        foreach ($groupIds as $key => $groupId) {
            $groupId = explode(', ', $groupId);
            foreach ($groupId as $id) {
                $tmp[$id] = $key;
            }
        }
        $groupIds = $tmp;
        unset($tmp);

        $this->_storage->setCatalogFieldsData('realSetIds', $groupIds, 'set');
    }
}