<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_FieldOption
 *
 * Notice: placeholders are skipped
 */
class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_FieldOption
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_CatalogFields_Model_FieldOption';
    protected $_magentoModelName  = 'eav/config';
    protected $_modelGroups      = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_ATTRIBUTE
    );

    protected $_uniqueKey = array(
        'field_id',
        'label'
    );

    protected $_lastAutoIncrementId = 1;

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'              => 'sellvana_autoincrement',
        'field_id'        => 'value',
        'label'           => 'label',
        //'locale'          => '',
        //'data_serialized' => ''
    );

    /** @var array map real option id to selvana id; */
    protected $_optionMap = array();

    /** @var array */
    protected $_optionLabels = array();

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'sellvana_autoincrement' => array('ruleVirtualAutoIncrement', 'ruleString'),
                /** @see ruleOptionUnique() */
                'label' => 'ruleOptionUnique'
            ),
            'skip' => array('skipField', 'skipSourceModel')
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _export()
    {
        /** @var Mage_Eav_Model_Config $attributes */
        $attributes = Mage::getModel($this->_magentoModelName);
        $attributes = $attributes
            ->getEntityType(Mage_Catalog_Model_Product::ENTITY)
            ->getAttributeCollection();

        if (!sizeof($attributes)) {
            return null;
        }

        /** @var Mage_Catalog_Model_Resource_Eav_Attribute $model */
        foreach ($attributes as $model) {
            if ($this->skip($model)) {
                continue;
            }

            $modelData = $this->_prepareData($model);

            $this->writeToFile($modelData);
        }

        $this->_storage->setCatalogFieldsData('option_map', $this->_optionMap, 'field_options');

        $this->_storage->setCatalogFieldsData('option_labels', $this->_optionLabels, 'field_options');

        return $this;
    }

    public $types;

    /**
     * @param Varien_Object|Mage_Catalog_Model_Resource_Eav_Attribute $model
     * @param null|array $map
     * @return array
     */
    protected function _prepareData(Varien_Object $model, $map = null)
    {
        //$this->log->start(get_class($this) . '-' . $model->getId());
        if (null === $map) {
            $map = $this->_defaultFieldsMap;
        }

        $options = array();
        if ($model->usesSource()) {
            $options = $model->getSource()->getAllOptions();
        }

        $rows = array();
        foreach ($options as $key => $option) {
            if ($option['value'] === '') {
                continue;
            }

            $tmpModel = new Varien_Object();
            $tmpModel->setData('value', $model->getId());
            $tmpModel->setData('label', $option['label']);

            $key = $model->getData('attribute_code') . '/' . $option['value'];

            $rows[] = parent::_prepareData($tmpModel, $map);

            $this->_optionMap[$key] = $tmpModel->getData('sellvana_autoincrement');
            $this->_optionLabels[$key] = $tmpModel->getData('label');
        }
        //$this->log->end(get_class($this) . '-' . $model->getId());
        return $rows;
    }

    /**
     * Set unique value of attribute.
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleOptionUnique(Varien_Object $model, $attribute)
    {
        $value = $model->getData($attribute);
        $tmpValue = trim($value);
        $i = 0;
        if (!array_key_exists($attribute, $this->_uniqueKeys)) {
            $this->_uniqueKeys[$attribute] = array();
        }

        do {
            if ($i > 0) {
                $tmpValue = $value . '-' . $i;
            }
            $i++;
            $prevCount = count($this->_uniqueKeys[$attribute]);
            $processedValue = $model->getData('value') . '/';
            if (function_exists('transliterator_transliterate')) {
                $processedValue .= transliterator_transliterate(
                    $this->_availableTransliterationRules,
                    $tmpValue
                );
            }
            $this->_uniqueKeys[$attribute][$processedValue] = true;
        } while (count($this->_uniqueKeys[$attribute]) == $prevCount);

        $this->_uniqueKeys[$attribute][$tmpValue] = true;
        $model->setData($attribute, $tmpValue);
        return true;
    }

    /**
     * @param Varien_Object $model
     * @return bool
     */
    public function skipField(Varien_Object $model)
    {
        if ($model->getData('is_user_defined') == 0) {
            return true;
        }

        $inputType = $model->getData('frontend_input');

        if (in_array($inputType, array('select', 'multiselect'))) {
            return false;
        }

        return true;
    }

    /**
     * @param Varien_Object $model
     * @return bool
     */
    public function skipSourceModel(Varien_Object $model)
    {
        $sourceModel = $model->getData('source_model');
        if (in_array($sourceModel, array(
            'core/design_source_design' //TODO: It is necessary to implement the logic to this model.
        ))) {
            return true;
        }

        return false;
    }
}