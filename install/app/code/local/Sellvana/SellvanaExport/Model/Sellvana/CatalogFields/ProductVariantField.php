<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVariantField
 */
class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVariantField
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_CatalogFields_Model_ProductVariantField';
    protected $_magentoModelName  = 'catalog/product';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_ATTRIBUTE,
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT
    );
    protected $_uniqueKey         = array(
        'product_id',
        'field_values'
    );

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap  = array(
        "id"            => 'sellvana_autoincrement',
        "product_id"    => 'parent_id',
        "variant_id"    => 'sellvana_variant_id',
        "field_id"      => 'sellvana_field_id',
        "varfield_id"   => 'sellvana_varfield_id',
        "option_id"     => 'sellvana_option_id'
    );

    /** @var int */
    protected $_counter           = 0;

    /**
     * @inheritdoc
     */
    public function _construct()
    {
        parent::_construct();

        /**
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable::_export,
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimple::_export
         */
        $this->setData('processed_products', $this->_storage->getProductData('processed', 'products'));

        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVarfield::_export */
        $this->setData('varfields', $this->_storage->getProductData('varfields', 'products'));

        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVariant::_export */
        $this->setData('variants', $this->_storage->getProductData('variants', 'products'));

        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_FieldOption::_export */
        $this->setData('option_map', $this->_storage->getCatalogFieldsData('option_map', 'field_options'));

        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_Field::_export */
        $this->setData('field_data', $this->_storage->getCatalogFieldsData('attribute_codes', 'field'));

        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_FieldOption::_export */
        $this->setData('option_labels', $this->_storage->getCatalogFieldsData('option_labels', 'field_options'));

        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVarfield::_export */
        $this->setData('config_attributes', $this->_storage->getProductData('config_attributes', 'products'));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection->addAttributeToFilter('type_id', array('in' => array('configurable')));
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'sellvana_autoincrement' => array('ruleVirtualAutoIncrement', 'ruleString'),
                'sellvana_variant_id'    => array('ruleVariantId'),
                'sellvana_varfield_id'   => array('ruleVarfieldId'),
                'sellvana_option_id'     => array('ruleOptionId'),
                'sellvana_field_id'      => array('ruleFieldId'),
            ),
            'skip' => 'skipProduct'
        ));
    }

    /**
     * @param Varien_Object $model
     * @param null $map
     * @return array
     */
    protected function _prepareData(Varien_Object $model, $map = null)
    {
        /** @var Mage_Catalog_Model_Product_Type_Configurable $typeInstance */
        $typeInstance = $model->getTypeInstance();
        $children = $typeInstance->getUsedProductCollection();
        $return = array();

        $configurableAttributes = $this->getData('config_attributes');
        if (!array_key_exists($model->getId(), $configurableAttributes)) {
            return $return;
        }

        $attributes = $configurableAttributes[$model->getId()];
        foreach ($attributes as $attribute) {
            $children->addAttributeToSelect($attribute['attribute_code']);
        }

        $optionLabels = $this->getData('option_labels');
        foreach ($children as $child) {
            /** @var Mage_Catalog_Model_Product $child */
            $fieldValues = array();
            foreach ($attributes as $attribute) {
                $code = $attribute['attribute_code'];
                $value = $code . '/' . $child->getData($code);
                if (!array_key_exists($value, $optionLabels)) {
                    continue;
                }
                $fieldValues[$code] = $optionLabels[$value];;
                $child->setData('sellvana_tmp_attribute_code', $code);
                $child->setData('sellvana_tmp_attribute_id', $attribute['attribute_id']);
                $return[] = parent::_prepareData($child, $map);
            }
        }

        if (($this->_counter++ % 50) == 0) {
            gc_collect_cycles();
        }

        return $return;
    }

    /**
     * Getting variant ID by product ID
     *
     * @param Mage_Catalog_Model_Product $model
     * @param $attribute
     * @return bool
     */
    public function ruleVariantId(Mage_Catalog_Model_Product $model, $attribute)
    {
        $variants = $this->getData('variants');
        if (array_key_exists($model->getId(), $variants)) {
            $model->setData($attribute, $variants[$model->getId()]);
        }

        return true;
    }

    /**
     * Getting variant ID by product ID
     *
     * @param Mage_Catalog_Model_Product $model
     * @param $attribute
     * @return bool
     */
    public function ruleVarfieldId(Mage_Catalog_Model_Product $model, $attribute)
    {
        $varfields = $this->getData('varfields');
        $attributeId = $model->getData('sellvana_tmp_attribute_id');

        if (
            array_key_exists($model->getParentId(), $varfields)
            && array_key_exists($attributeId, $varfields[$model->getParentId()])
        ) {
            $model->setData($attribute, $varfields[$model->getParentId()][$attributeId]);
        }

        return true;
    }

    /**
     * Get option increment ID by Magento option ID and attribute code
     *
     * @param Mage_Catalog_Model_Product $model
     * @param $attribute
     * @return bool
     */
    public function ruleOptionId(Mage_Catalog_Model_Product $model, $attribute)
    {
        $attributeCode = $model->getData('sellvana_tmp_attribute_code');
        $key = $attributeCode . '/' . $model->getData($attributeCode);
        $optionMap = $this->getData('option_map');

        if (array_key_exists($key, $optionMap)) {
            $model->setData($attribute, $optionMap[$key]);
        }

        return true;
    }

    /**
     * Get field increment ID by Magento attribute code
     *
     * @param Mage_Catalog_Model_Product $model
     * @param $attribute
     * @return bool
     */
    public function ruleFieldId(Mage_Catalog_Model_Product $model, $attribute)
    {
        $attributeCode = $model->getData('sellvana_tmp_attribute_code');
        $fieldData = $this->getData('field_data');

        if (array_key_exists($attributeCode, $fieldData)) {
            $model->setData($attribute, $fieldData[$attributeCode]['id']);
        }

        return true;
    }

    /**
     * @param Mage_Catalog_Model_Product $model
     * @return bool
     */
    public function skipProduct(Mage_Catalog_Model_Product $model)
    {
        $processedProducts = $this->getData('processed_products');

        $id = $model->getId();
        return !(
            array_key_exists($id, $processedProducts)
            && $processedProducts[$id] == 'configurable'
        );
    }

}