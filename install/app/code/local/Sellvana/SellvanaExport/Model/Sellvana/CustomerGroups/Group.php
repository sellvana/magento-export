<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_CustomerGroups_Group
 */
class Sellvana_SellvanaExport_Model_Sellvana_CustomerGroups_Group
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName        = 'Sellvana_CustomerGroups_Model_Group';
    protected $_magentoModelName = 'customer/group';
    protected $_modelGroups      = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_CUSTOMER,
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_CUSTOMER_GROUP
    );
    protected $_uniqueKey        = 'code';

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id' => 'PK',
        'title' => 'customer_group_code',
        'code' => 'sellvana_code'
    );

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'sellvana_code' => 'ruleGroupCode'
            ),
            'skip' => 'skipGroup'
        ));
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleGroupCode(Varien_Object $model, $attribute)
    {
        $code = strtolower($model->getData('customer_group_code'));
        $code = str_replace(' ', '_', $code);

        $model->setData($attribute, $code);

        return true;
    }


    /**
     * @param Varien_Object $model
     * @return bool
     */
    public function skipGroup(Varien_Object $model)
    {
        if ($model->getId() == 0) {
            return true;
        }
        return false;
    }
}