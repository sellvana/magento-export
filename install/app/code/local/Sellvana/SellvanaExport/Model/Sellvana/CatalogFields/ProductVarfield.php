<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVarfield
 */
class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVarfield
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_CatalogFields_Model_ProductVarfield';
    protected $_magentoModelName  = 'catalog/product';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_ATTRIBUTE,
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT
    );
    protected $_uniqueKey         = array(
        'product_id',
        'field_id'
    );

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap  = array(
        'id'          => 'sellvana_autoincrement',
        'product_id'  => 'PK',
        'field_id'    => 'sellvana_field_id',
        'field_label' => 'sellvana_field_label',
        'position'    => 'sellvana_position'
    );

    /** @var array */
    protected $_varfields         = array();

    /** @var array */
    protected $_configAttributes = array();

    public function _construct()
    {
        parent::_construct();

        /**
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable::_export,
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimple::_export
         */
        $this->setData('processed_products', $this->_storage->getProductData('processed', 'products'));

        $resource = Mage::getSingleton('core/resource');
        $configurableAttributes = Mage::getSingleton('eav/config')
            ->getEntityType(Mage_Catalog_Model_Product::ENTITY)
            ->getAttributeCollection();
        $configurableAttributes->getSelect()->join(
            array('attr' => $resource->getTableName('catalog/product_super_attribute')),
            'attr.attribute_id = main_table.attribute_id',
            array('product_id', 'config_attr_position' => 'position')
        )->order('attr.position ASC');
        $select = $configurableAttributes->getSelect();
        $result = $resource->getConnection('core_write')->query($select);
        $rows = $result->fetchAll();

        foreach ($rows as $attribute) {
            $productId = $attribute['product_id'];
            if (!array_key_exists($productId, $this->_configAttributes)) {
                $this->_configAttributes[$productId] = array();
            }

            $this->_configAttributes[$productId][] = array(
                'attribute_id'   => $attribute['attribute_id'],
                'attribute_code' => $attribute['attribute_code'],
                'frontend_label' => $attribute['frontend_label'],
                'position'       => $attribute['config_attr_position'],
            );
        }
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'sellvana_autoincrement' => array('ruleVirtualAutoIncrement', 'ruleString')
            ),
            'skip' => 'skipProduct'
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection->addAttributeToFilter('type_id', array('in' => array('configurable')));
    }

    /**
     * @inheritdoc
     */
    protected function _export()
    {
        $return = parent::_export();
        $this->_storage->setProductData('varfields', $this->_varfields, 'products');
        $this->_storage->setProductData('config_attributes', $this->_configAttributes, 'products');
        return $return;
    }

    protected function _prepareData(Varien_Object $model, $map = null)
    {
        $return = array();

        $tmpModel = clone $model;
        if (!array_key_exists($model->getId(), $this->_configAttributes)) {
            return $return;
        }

        $configurableAttributes = $this->_configAttributes[$model->getId()];
        foreach ($configurableAttributes as $attribute) {
            $attributeId = $attribute['attribute_id'];

            $tmpModel->setData('sellvana_field_id', $attributeId);
            $tmpModel->setData('sellvana_field_label', $attribute['frontend_label']);
            $tmpModel->setData('sellvana_position', $attribute['position']);

            $return[] = parent::_prepareData($tmpModel, $map);

            if (!array_key_exists($model->getId(), $this->_varfields)) {
                $this->_varfields[$model->getId()] = array();
            }

            $this->_varfields[$model->getId()][$attributeId] = $tmpModel->getData('sellvana_autoincrement');
        }

        return $return;
    }


    /**
     * @param Mage_Catalog_Model_Product $model
     * @return bool
     */
    public function skipProduct(Mage_Catalog_Model_Product $model)
    {
        $processedProducts = $this->getData('processed_products');

        $id = $model->getId();
        $skip = !(
            array_key_exists($id, $processedProducts)
            && $processedProducts[$id] == 'configurable'
        );
        $processedProducts = null;
        unset($processedProducts);

        return $skip;
    }

}