<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductPrice
 */
class Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductPrice
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    const TYPE_BASE   = "base",
          TYPE_MAP    = "map",
          TYPE_MSRP   = "msrp",
          TYPE_SALE   = "sale",
          TYPE_TIER   = "tier",
          TYPE_COST   = "cost",
          TYPE_PROMO  = "promo";

    protected $_sellvanaModelName = 'Sellvana_Catalog_Model_ProductPrice';
    protected $_magentoModelName  = 'catalog/product';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT
    );
    protected $_uniqueKey        = array(
        'product_id',
        'price_type',
        'customer_group_id',
        'site_id',
        'currency_code',
        'qty',
        'variant_id',
        'promo_id'
    );

    protected $_lastAutoIncrementId = 1;

    protected $_globalMsrpData;

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'                => 'virtual_auto_increment',
        'product_id'        => 'PK',
        'customer_group_id' => 'sellvana_customer_group_id',
        'site_id'           => 'sellvana_site_id',
        'amount'            => 'sellvana_amount',
        'price_type'        => 'sellvana_price_type',
        'qty'               => 'sellvana_price_qty',
        'currency_code'     => 'sellvana_currency_code',
        //'data_serialized'   => '',
        'valid_from'        => 'sellvana_valid_from',
        'valid_to'          => 'sellvana_valid_to',
        'operation'         => 'sellvana_operation',
        //'base_field'        => '',
        'variant_id'        => 'sellvana_variant_id',
        'promo_id'          => 'sellvana_promo_id'
    );

    protected $_priceTypes = array(
        self::TYPE_BASE  => "Base Price",
        //self::TYPE_MAP   => "MAP",
        //self::TYPE_MSRP  => "MSRP",
        self::TYPE_SALE  => "Sale Price",
        self::TYPE_TIER  => "Tier Price",
        //self::TYPE_COST  => "Cost", // Don't exist in Magento
        //self::TYPE_PROMO => "Promo Price",
    );

    protected $_currentPriceType;

    /**
     * @inheritdoc
     */
    public function _construct()
    {
        parent::_construct();

        /**
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable::_export,
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimple::_export
         */
        $this->setData('processed_products', $this->_storage->getProductData('processed', 'products'));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection->addAttributeToFilter('type_id', array('in' => array('simple', 'configurable', 'bundle', 'virtual')));
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'virtual_auto_increment' => array('ruleVirtualAutoIncrement', 'ruleString'),
                'sellvana_operation'     => 'ruleOperation',
                'sellvana_price_qty'     => array('rulePriceQty', 'ruleInt', 'ruleString')
            ),
            'skip' => 'skipProduct'
        ));
    }

    /**
     * @param Varien_Object|Mage_Catalog_Model_Product $model
     * @param null|array $map
     * @return array
     */
    protected function _prepareData(Varien_Object $model, $map = null)
    {
        //$this->log->start(get_class($this) . '-' . $model->getId());
        if (null === $map) {
            $map = $this->_defaultFieldsMap;
        }

        $result = array();
        foreach ($this->_priceTypes as $key => $name) {
            $this->_currentPriceType = $key;
            $tmpModel = clone $model;
            switch ($key) {
                case self::TYPE_BASE:
                    $result = array_merge($result, $this->_prepareBasePrices($tmpModel, $map));
                    break;
                case self::TYPE_SALE:
                    $result = array_merge($result, $this->_prepareSalePrices($tmpModel, $map));
                    break;
                case self::TYPE_TIER:
                    $result = array_merge($result, $this->_prepareTierPrices($tmpModel, $map));
                    break;
                case  self::TYPE_MAP:
                case  self::TYPE_MSRP:
                    $result = array_merge($result, $this->_prepareMapPrices($tmpModel, $map));
                    break;
            }
        }

        //$this->log->end(get_class($this) . '-' . $model->getId());
        return $result;
    }

    /**
     * @param Mage_Catalog_Model_Product $model
     * @param $map
     * @return array
     */
    protected function _prepareBasePrices(Mage_Catalog_Model_Product $model, $map)
    {
        $return = array();

        /** @var $dbi Varien_Db_Adapter_Interface */
        $dbi = Mage::getSingleton('core/resource')->getConnection('core_read');

        $rows = $this->_getEavMappedData($dbi, $this->_getEavDecimalSelect($dbi, 'price', $model));

        foreach ($rows as $groupId => $value) {
            $tmpModel = clone $model;
            $tmpModel->setData('sellvana_amount', $value);
            $tmpModel->setData('sellvana_site_id', $this->_storage->getMultisiteData($groupId, 'store'));
            $tmpModel->setData('sellvana_price_type', $this->_currentPriceType);

            $result = array();
            foreach ($map as $field => $attribute) {
                $result[$field] = $this->getValidatedData($attribute, $tmpModel);
            }
            $return[] = json_encode(array_values($result));
        }

        return $return;
    }

    /**
     * @param Mage_Catalog_Model_Product $model
     * @param $map
     * @return array
     */
    protected function _prepareSalePrices(Mage_Catalog_Model_Product $model, $map)
    {
        $return = array();

        /** @var $dbi Varien_Db_Adapter_Interface */
        $dbi = Mage::getSingleton('core/resource')->getConnection('core_read');

        $specialPrice = $this->_getEavMappedData(
            $dbi, $this->_getEavDecimalSelect($dbi, 'special_price', $model)
        );

        $skip = true;
        foreach ($specialPrice as $value) {
            if (null !== $value) {
                $skip = false;
            }
        }

        if ($skip) {
            return $return;
        }

        $specialFrom = $this->_getEavMappedData(
            $dbi, $this->_getEavDataTimeSelect($dbi, 'special_from_date', $model)
        );

        $specialTo = $this->_getEavMappedData(
            $dbi, $this->_getEavDataTimeSelect($dbi, 'special_to_date', $model)
        );

        $passes = array_unique(array_merge(
            array_keys($specialPrice),
            array_keys($specialFrom),
            array_keys($specialTo)
        ));

        foreach ($passes as $groupId) {
            $amount = array_key_exists($groupId, $specialPrice) ? $specialPrice[$groupId] : $specialPrice[0];
            $validFrom = array_key_exists($groupId, $specialFrom) ? $specialFrom[$groupId] : $specialFrom[0];
            $validTo = array_key_exists($groupId, $specialTo) ? $specialTo[$groupId] : $specialTo[0];

            $tmpModel = clone $model;

            $tmpModel->setData('sellvana_amount', $amount);
            $tmpModel->setData('sellvana_valid_from', $validFrom);
            $tmpModel->setData('sellvana_valid_to', $validTo);
            $tmpModel->setData('sellvana_site_id', $this->_storage->getMultisiteData($groupId, 'store'));
            $tmpModel->setData('sellvana_price_type', $this->_currentPriceType);

            $result = array();
            foreach ($map as $field => $attribute) {
                $result[$field] = $this->getValidatedData($attribute, $tmpModel);
            }

            $return[] = json_encode(array_values($result));
        }

        return $return;
    }

    /**
     * @param Mage_Catalog_Model_Product $model
     * @param $map
     * @return array
     */
    protected function _prepareTierPrices(Mage_Catalog_Model_Product $model, $map)
    {
        $return = array();

        /** @var $dbi Varien_Db_Adapter_Interface */
        $dbi = Mage::getSingleton('core/resource')->getConnection('core_read');

        $resource = Mage::getSingleton('core/resource');

        $select = $dbi->select();

        $select->from(array("main_table" => $resource->getTableName('catalog/product_attribute_tier_price')))
            ->where('main_table.entity_id = ?', $model->getId());

        $rows = $dbi->fetchAll($select);

        if (empty($rows)) {
            return $return;
        }

        foreach ($rows as $row) {
            $tmpModel = clone $model;

            if (!$row['all_groups']) {
                $tmpModel->setData('sellvana_customer_group_id', $row['customer_group_id']);
            }
            $tmpModel->setData('sellvana_amount', $row['value']);
            $tmpModel->setData('sellvana_price_qty', $row['qty']);
            $tmpModel->setData('sellvana_site_id', $this->_storage->getMultisiteData($row['website_id'], 'store'));
            $tmpModel->setData('sellvana_price_type', $this->_currentPriceType);

            $result = array();
            foreach ($map as $field => $attribute) {
                $result[$field] = $this->getValidatedData($attribute, $tmpModel);
            }

            $return[] = json_encode(array_values($result));
        }

        return $return;
    }

    /**
     * @param Mage_Catalog_Model_Product $model
     * @param $map
     * @return array
     */
    protected function _prepareMapPrices(Mage_Catalog_Model_Product $model, $map)
    {
        $return = array();

        /** @var Sellvana_SellvanaExport_Helper_Array $arrayHelper */
        $arrayHelper = Mage::helper('sellvana_sellvanaexport/array');

        $msrp = $model->getData('msrp');
        if (null === $msrp) {
            return $return;
        }

        $globalMsrpEnabled = $this->_globalMsrpData['enabled'];

        if (null === $this->_globalMsrpData) {
            $config = Mage::getModel('core/config_data')->getCollection();
            $config->addFieldToFilter('path', array('like' => 'sales/msrp/enabled'));
            $globalMsrpEnabled = $config->load()->toArray();
            $globalMsrpEnabled = $arrayHelper->map($globalMsrpEnabled['items'], 'scope_id', 'value', 'scope');
            $globalMsrpEnabled = $arrayHelper->get($globalMsrpEnabled, 'default', array(0 => 0));
            $globalMsrpEnabled = $globalMsrpEnabled[0];

            $config = Mage::getModel('core/config_data')->getCollection();
            $config->addFieldToFilter('path', array('like' => 'sales/msrp/apply_for_all'));
            $globalMsrp = $config->load()->toArray();
            $globalMsrp = $arrayHelper->map($globalMsrp['items'], 'scope_id', 'value', 'scope');

            $this->_globalMsrpData = array(
                'enabled' => $globalMsrpEnabled,
                'value' => $globalMsrp
            );
        }

        if (!$globalMsrpEnabled) {
            //TODO: do we need to export if MSRP is disabled globally?
        }

        /** @var $dbi Varien_Db_Adapter_Interface */
        $dbi = Mage::getSingleton('core/resource')->getConnection('core_read');

        $rows = $this->_getEavMappedData($dbi, $this->_getEavVarcharSelect($dbi, 'msrp_enabled', $model));

        foreach ($rows as $groupId => $value) {
            switch ($value) {
                case 0:
                    return $return;
                    break;
                case 1:
                    break;
                case 2:
                    $tmpEnabled = $arrayHelper->get($globalMsrpEnabled, 'websites', array());

                    $hasConfig = array_key_exists($groupId, $tmpEnabled);
                    $tmpEnabled = $hasConfig ? $tmpEnabled[$groupId] : $globalMsrpEnabled['default'][0];
                    if (!$tmpEnabled) {
                        return $return;
                    }
                    break;
            }

            $tmpModel = clone $model;
            $tmpModel->setData('sellvana_amount', $msrp);
            $tmpModel->setData('sellvana_site_id', $this->_storage->getMultisiteData($groupId, 'store'));
            $tmpModel->setData('sellvana_price_type', $this->_currentPriceType);

            $result = array();
            foreach ($map as $field => $attribute) {
                $result[$field] = $this->getValidatedData($attribute, $tmpModel);
            }
            $return[] = json_encode(array_values($result));
        }

        return $return;
    }

    /**
     * @param Varien_Db_Adapter_Interface $dbi
     * @param string $dbName
     * @return Varien_Db_Select
     */
    protected function _getEavSelect(Varien_Db_Adapter_Interface $dbi, $dbName)
    {
        $select = $dbi->select();

        $select->from(
            array("main_table" => $dbi->getTableName($dbName)),
            array('value')
        );

        return $select;
    }

    /**
     * @param Varien_Db_Adapter_Interface $dbi
     * @param string $dbName
     * @param string $attribute
     * @param Varien_Object $model
     * @return Varien_Db_Select
     */
    protected function _prepareEavSelect(
        Varien_Db_Adapter_Interface $dbi,
        $dbName,
        $attribute,
        Varien_Object $model
    ) {
        $resource = Mage::getSingleton('core/resource');

        $select = $this->_getEavSelect($dbi, $dbName);

        $select->joinInner(
            array('cs_table' => $resource->getTableName('core/store')),
            'main_table.store_id = cs_table.store_id',
            array('group_id')
        )
            ->joinInner(
                array('eav_table' => $resource->getTableName('eav/attribute')),
                'main_table.attribute_id = eav_table.attribute_id',
                array('')
            )
            ->where('eav_table.attribute_code = ?', $attribute)
            ->where('main_table.entity_id = ?', $model->getId())
            ->group('cs_table.group_id');

        return $select;
    }

    /**
     * @param Varien_Db_Adapter_Interface $dbi
     * @param string $attribute
     * @param Varien_Object $model
     * @return Varien_Db_Select
     */
    protected function _getEavDecimalSelect(
        Varien_Db_Adapter_Interface $dbi,
        $attribute,
        Varien_Object $model
    ) {
        return $this->_prepareEavSelect($dbi, 'catalog_product_entity_decimal', $attribute, $model);
    }

    /**
     * @param Varien_Db_Adapter_Interface $dbi
     * @param string $attribute
     * @param Varien_Object $model
     * @return Varien_Db_Select
     */
    protected function _getEavDataTimeSelect(
        Varien_Db_Adapter_Interface $dbi,
        $attribute,
        Varien_Object $model
    ) {
        return $this->_prepareEavSelect($dbi, 'catalog_product_entity_datetime', $attribute, $model);
    }

    /**
     * @param Varien_Db_Adapter_Interface $dbi
     * @param $attribute
     * @param Varien_Object $model
     * @return Varien_Db_Select
     */
    protected function _getEavVarcharSelect(
        Varien_Db_Adapter_Interface $dbi,
        $attribute,
        Varien_Object $model
    ) {
        return $this->_prepareEavSelect($dbi, 'catalog_product_entity_varchar', $attribute, $model);
    }

    /**
     * @param Varien_Db_Adapter_Interface $dbi
     * @param Varien_Db_Select $select
     * @return array
     */
    protected function _getEavMappedData(Varien_Db_Adapter_Interface $dbi, Varien_Db_Select $select)
    {
        $rows = $dbi->fetchAll($select);

        $arrayHelper = Mage::helper('sellvana_sellvanaexport/array');
        $rows = $arrayHelper->map($rows, 'group_id', 'value');

        return $rows;
    }

    /**
     * Check if is necessary to skip product
     *
     * @param Mage_Catalog_Model_Product $model
     * @return bool
     */
    public function skipProduct(Mage_Catalog_Model_Product $model)
    {
        $processedProducts = $this->getData('processed_products');
        $productId = $model->getId();

        if (!array_key_exists($productId, $processedProducts)) {
            return true;
        }

        $type = $model->getData('type_id');
        return in_array($type, array(
            'bundle',
            //'configurable',
            //'downloadable',
            'grouped',
            //'simple',
            //'virtual'
        ));


    }

    /**
     * @param Varien_Object|Mage_Core_Model_Store $model
     * @param $attribute
     * @return bool
     */
    public function ruleOperation(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, '=$');//Fixed
        return true;
    }

    /**
     * @param Varien_Object|Mage_Core_Model_Store $model
     * @param $attribute
     * @return bool
     */
    public function rulePriceQty(Varien_Object $model, $attribute)
    {
        $qty = 1;

        if ($this->_currentPriceType === self::TYPE_TIER) {
            $qty = $model->getData($attribute);
        }

        $model->setData($attribute, $qty);

        return true;
    }
}