<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_SalesTax_Rule
 */
class Sellvana_SellvanaExport_Model_Sellvana_SalesTax_Rule extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_SalesTax_Model_Rule';
    protected $_magentoModelName  = 'tax/calculation_rule';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_TAX
    );
    protected $_uniqueKey        = 'title';

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'                         => 'PK',//"1"
        'title'                      => 'code',//"NY State"
        'match_all_zones'            => 'zero',//"0"
        'match_all_customer_classes' => 'zero',//"0"
        'match_all_product_classes'  => 'zero',//"0"
        'compound_priority'          => 'priority',//"0"
        'sort_order'                 => 'position',//"0"
        'apply_to_shipping'          => 'zero',//"0"
        //'rule_rate_percent'          => '',//"4.0000"
        //'fpt_amount'                 => '',//null
        //'create_at'                  => '',//"2015-10-07 11:49:26"
        //'update_at'                  => ''//"2015-10-07 11:49:26"
    );

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'zero' => array('ruleZero', 'ruleString')
            )
        ));
    }
}