<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <vadim.buchinsky@gmail.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_MultiSite_Site
 *
 * This model is made to transform Website|Store|Store View hierarchy into Sellvana format.
 * Sellvana website is created for every store/store view if it has unique base url.
 * Exception: default store view for default website is skipped
 *
 * Attention: This model does not work on cases when similar Store View (e.g. website languages)
 * belong to different Stores and have different attribute values for products.
 * In this case only the values from the first processed Store View will be used
 *
 * @see Sellvana_SellvanaExport_Model_Sellvana_Storage::setMultisiteData
 * @see Sellvana_SellvanaExport_Model_Sellvana_Storage::getMultisiteData
 */
class Sellvana_SellvanaExport_Model_Sellvana_MultiSite_Site extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_MultiSite_Model_Site';
    protected $_magentoModelName = 'core/website';
    protected $_modelGroups      = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_WEBSITES
    );
    protected $_uniqueKey = array(
        'name',
        'root_category_id'
    );
    protected $_sites = array();
    protected $_baseUrls;

    /** @var  null|Mage_Core_Model_Website */
    protected $_currentWebsite;
    protected $_currentGroups;

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'               => 'PK',
        'name'             => 'name',
        //'match_domains'    => '',//ask: Return to that when sellvana multisite will work with both domains and paths
        //'default_theme'    => '',
        //'layout_update'    => '',
        'root_category_id' => 'root_category_id',
        //'data_serialized'  => '',
        //'create_at'        => '',
        //'update_at'        => '',
        //'home_url'         => ''//ask: Return to that when sellvana multisite will work with both domains and paths
    );

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'PK'   => array('ruleVirtualAutoIncrement', 'ruleString'),
                'name' => array('ruleCheckName', 'ruleString'),
                'root_category_id' => array('ruleCategoryId', 'ruleString'),
            ),
            'skip' => 'skipWebsite'
        ));
    }

    /**
     * @param Mage_Core_Model_Website $model
     * @param null $map
     * @return array|string
     */
    protected function _prepareData(Varien_Object $model, $map = null)
    {
        //$this->log->start(get_class($this) . '-' . $model->getId());
        if (null === $map) {
            $map = $this->_defaultFieldsMap;
        }

        //$model->getIsDefault();
        //$model->getDefaultStore();

        $stores = $model->getStores();

        $this->_currentWebsite = $model;
        $this->_currentGroups = $model->getGroups();

        $sites = array();
        /** @var Mage_Core_Model_Store $store */
        foreach ($stores as $store) {
            if ($this->skipStore($store)) {
                continue;
            }
            $result = array();
            foreach ($map as $field => $attribute) {
                $result[$field] = $this->getValidatedData($attribute, $store);
            }

            $this->_sites[$result['id']] = $result;
            $this->_storage->setMultisiteData($store->getId(), $result['id'], 'storeView');

            if ($this->isDefaultForWebsite($store)) {
                $this->_storage->setMultisiteData($model->getId(), $result['id'], 'website');
            }

            if ($this->isDefaultForGroup($store)) {
                $this->_storage->setMultisiteData($store->getGroupId(), $result['id'], 'store');
            }

            $sites[] = json_encode(array_values($result));
        }


        foreach ($stores as $store) {
            if (!$this->skipStore($store)) {
                continue;
            }
            $groupId = $store->getGroupId();
            $this->_storage->setMultisiteData(
                $store->getId(),
                $this->_storage->getMultisiteData($groupId, 'store'),
                'storeView'
            );
        }

        //$this->log->end(get_class($this) . '-' . $model->getId());

        return $sites;
    }

    /**
     * Check if current store view is default for store
     *
     * @param Mage_Core_Model_Store $model
     * @return bool
     */
    public function isDefaultForGroup(Mage_Core_Model_Store $model)
    {
        $id = $model->getId();

        $group = $model->getGroup();
        $groupDefaultStoreId = $group->getData('default_store_id');

        return $id == $groupDefaultStoreId;
    }

    /**
     * @param Mage_Core_Model_Store $model
     * @return bool
     */
    public function isDefaultForWebsite(Mage_Core_Model_Store $model)
    {
        $group = $model->getGroup();
        $groupId = $group->getId();

        $website = $this->_currentWebsite;
        $websiteDefaultGroupId = $website->getData('default_group_id');

        return ($this->isDefaultForGroup($model) && $groupId == $websiteDefaultGroupId);
    }

    /**
     * Skip backend
     *
     * @param Mage_Core_Model_Website $model
     * @return bool
     */
    public function skipWebsite(Mage_Core_Model_Website $model)
    {
        return $model->getData('code') == 'admin';
    }

    /**
     * Skip store if not need create separate site
     *
     * @param Mage_Core_Model_Store $model
     * @return bool
     */
    public function skipStore(Mage_Core_Model_Store $model)
    {
        $isDefaultForWebsite = $this->isDefaultForWebsite($model);
        $isDefaultForGroup = $this->isDefaultForGroup($model);

        // skip if store view is default for default Website
        if ($isDefaultForGroup && $this->_currentWebsite->getIsDefault()) {
            return true;
        }

        // don't skip if store view is default for any non-default website or store
        if ($isDefaultForGroup || $isDefaultForWebsite) {
            return false;
        }

        $defaultGroupCategoryId = $this->_currentWebsite->getDefaultGroup()->getData('root_category_id');
        $currentGroupCategoryId = $model->getGroup()->getData('root_category_id');
        // don't skip if store root category is different from website root category
        if ($defaultGroupCategoryId != $currentGroupCategoryId) {
            return false;
        }

        /** @var Sellvana_SellvanaExport_Helper_Array $arrayHelper */
        $arrayHelper = Mage::helper('sellvana_sellvanaexport/array');

        /** @var Mage_Core_Model_Resource_Config_Data_Collection $config */
        if (null === $this->_baseUrls) {
            $config = Mage::getModel('core/config_data')->getCollection();
            $config->addFieldToFilter('path', array('like' => 'web/%%/base_url'));

            $baseUrls = $config->load()->toArray();
            $baseUrls = $arrayHelper->map($baseUrls['items'], 'scope_id', 'value', 'scope');

            $this->_baseUrls = $baseUrls;
        } else {
            $baseUrls = $this->_baseUrls;
        }

        $defaultUrl = $arrayHelper->get($baseUrls, 'default', array());
        $defaultUrl = array_key_exists(0, $defaultUrl) ? $defaultUrl[0] : null;

        $storeUrls = $arrayHelper->get($baseUrls, 'stores', array());
        $storeUrl = array_key_exists($model->getId(), $storeUrls) ? $storeUrls[$model->getId()] : null;
        $arrayHelper->remove($storeUrls, $model->getId());

        // skip if Store View → Base URL is the same as Website Base URL
        if (null === $storeUrl || $defaultUrl == $storeUrl) {
            return true;
        }

        // skip if Store View → Base URL is not unique
        $allBaseUrls = $arrayHelper->get($baseUrls, 'websites', array());
        if (array_search($storeUrl, $allBaseUrls) !== false) {
            return true;
        }

        $replays = array_keys($allBaseUrls, $storeUrl);
        foreach ($replays as $storeId) {
            if (null !== $this->_storage->getMultisiteData($storeId, 'storeView')) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Mage_Core_Model_Store $model
     * @param $attribute
     * @return bool
     */
    public function ruleVirtualAutoIncrement(Varien_Object $model, $attribute)
    {
        $lastKey = key(array_slice($this->_sites, -1, 1, TRUE));
        if (null === $lastKey) {
            $lastKey = 0;
        }

        $model->setData($attribute, ++$lastKey);

        return true;
    }

    /**
     * @param Mage_Core_Model_Store $model
     * @param $attribute
     * @return bool
     */
    public function ruleCheckName(Varien_Object $model, $attribute)
    {
        $group = $model->getGroup();
        $website = $this->_currentWebsite;

        $isDefaultForWebsite = $this->isDefaultForWebsite($model);
        $isDefaultForGroup = $this->isDefaultForGroup($model);

        if ($isDefaultForWebsite) {
            $name = $website->getData('name');
        } elseif ($isDefaultForGroup && !$isDefaultForGroup) {
            $name = $website->getData('name')
                . ' | ' .
                $group->getData('name');
        } else {
            $name = $website->getData('name')
                . ' | ' .
                $group->getData('name')
                . ' | ' .
                $model->getData('name');
        }

        $model->setData($attribute, $name);
        return true;
    }

    /**
     * @param Mage_Core_Model_Store $model
     * @param $attribute
     * @return bool
     */
    public function ruleCategoryId(Varien_Object $model, $attribute)
    {
        $categoryId = $model->getGroup()->getData('root_category_id');
        $model->setData($attribute, $categoryId);

        return true;
    }
}