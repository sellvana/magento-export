<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVariant
 */
class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVariant
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_CatalogFields_Model_ProductVariant';
    protected $_magentoModelName  = 'catalog/product';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_ATTRIBUTE,
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT
    );
    protected $_uniqueKey         = array(
        'product_id',
        'field_values'
    );

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap  = array(
        "id"            => 'sellvana_autoincrement',
        "product_id"    => 'parent_id',
        "field_values"  => 'sellvana_field_values',
        "product_sku"   => 'sku',
        "inventory_sku" => 'sku',
        //"variant_price" => '',
        //"data_serialized" => ''
    );

    /** @var array */
    protected $_variants          = array();

    /** @var int */
    protected $_counter           = 0;



    /**
     * @inheritdoc
     */
    public function _construct()
    {
        parent::_construct();

        /**
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable::_export,
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimple::_export
         */
        $this->setData('processed_products', $this->_storage->getProductData('processed', 'products'));

        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVarfield::_export */
        $this->setData('config_attributes', $this->_storage->getProductData('config_attributes', 'products'));

        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_FieldOption::_export */
        $this->setData('option_labels', $this->_storage->getCatalogFieldsData('option_labels', 'field_options'));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection->addAttributeToFilter('type_id', array('in' => array('configurable')));
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'sellvana_autoincrement' => array('ruleVirtualAutoIncrement', 'ruleString')
            ),
            /** @see skipProduct() */
            'skip' => 'skipProduct'
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _export()
    {
        $return = parent::_export();
        $this->_storage->setProductData('variants', $this->_variants, 'products');
        return $return;
    }

    /**
     * @param Varien_Object $model
     * @param null $map
     * @return array
     */
    protected function _prepareData(Varien_Object $model, $map = null)
    {
        /** @var Mage_Catalog_Model_Product_Type_Configurable $typeInstance */
        $typeInstance = $model->getTypeInstance();
        $children = $typeInstance->getUsedProductCollection();
        $return = array();

        $configurableAttributes = $this->getData('config_attributes');
        if (!array_key_exists($model->getId(), $configurableAttributes)) {
            return $return;
        }

        $attributes = $configurableAttributes[$model->getId()];
        foreach ($attributes as $attribute) {
            $children->addAttributeToSelect($attribute['attribute_code']);
        }

        $optionLabels = $this->getData('option_labels');
        foreach ($children as $child) {
            /** @var Mage_Catalog_Model_Product $child */
            $fieldValues = array();
            foreach ($attributes as $attribute) {
                $code = $attribute['attribute_code'];
                $value = $code . '/' . $child->getData($code);
                if (!array_key_exists($value, $optionLabels)) {
                    continue;
                }

                $fieldValues[$code] = $optionLabels[$value];
            }
            $child->setData('sellvana_field_values', json_encode($fieldValues));

            $return[] = parent::_prepareData($child, $map);

            $this->_variants[$child->getId()] = $child->getData('sellvana_autoincrement');
        }

        if (($this->_counter++ % 50) == 0) {
            gc_collect_cycles();
        }

        return $return;
    }

    /**
     * @param Mage_Catalog_Model_Product $model
     * @return bool
     */
    public function skipProduct(Mage_Catalog_Model_Product $model)
    {
        $processedProducts = $this->getData('processed_products');

        $id = $model->getId();
        return !(
            array_key_exists($id, $processedProducts)
            && $processedProducts[$id] == 'configurable'
        );
    }

}