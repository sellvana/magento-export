<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductBundle
 */
class Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductBundle
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_Catalog_Model_Product';
    protected $_magentoModelName  = 'catalog/product';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT,
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_ATTRIBUTE
    );
    protected $_uniqueKey        = 'product_sku';

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'                => 'PK',
        'product_sku'       => 'sku', //Sellvana requires a unique and not null value
        'product_name'      => 'name',
        'short_description' => 'short_description',
        'description'       => 'description',
        'url_key'           => 'url_key', //Sellvana requires a unique and not null value
        //'net_weight'        => null,
        //'ship_weight'       => null,
        'is_hidden'         => 'status', //ask: status or visibility
        //'notes'             => null,
        //'uom'               => 'EACH', //ask: find out what that is
        //'thumb_url'         => null,
        //'images_data'       => null,
        //'data_serialized'   => null,
        //'is_featured'       => null,
        //'is_popular'        => null,
        //'position'          => null,
        'inventory_sku'     => 'inventory_sku',
        //'manage_inventory'  => '0',//ask: find out what that is
        //'avg_rating'        => null,
        //'num_reviews'       => null
        'custom_details_view' => 'sellvana_custom_details_view',
        '_custom_data'        => 'sellvana_custom_data',
    );

    protected $_typeMap = array(
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_FIELD        => 'text',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_AREA         => 'textarea',
        //Mage_Catalog_Model_Product_Option::OPTION_TYPE_FILE         => '',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_DROP_DOWN    => 'select',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_RADIO        => 'select',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_CHECKBOX     => 'checkbox',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_MULTIPLE     => 'text',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_DATE         => 'text',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_DATE_TIME    => 'text',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_TIME         => 'text'
    );


    /** @var array ids of products included in the export */
    protected $_processed = array();

    /** @var array */
    protected $_storeLocales = array();

    /** @var array */
    protected $_fieldLabelTranslations = array();

    /** @var array */
    protected $_itemNameTranslations = array();

    /** @var array  */
    protected static $_translatableFields = array(
        'product_name_lang_fields' => 'name',
        'short_description_lang_fields' => 'short_description',
        'description_lang_fields' => 'description'
    );

    /** @var array  */
    protected static $_translatableFieldTypes = array(
        'product_name_lang_fields' => 'text',
        'short_description_lang_fields' => 'textarea',
        'description_lang_fields' => 'wysiwyg'
    );

    /** @var array */
    protected $_textFieldTranslations = array();

    /** @var array  */
    protected $_attributeCodeById = array();

    /**
     * @inheritdoc
     */
    public function _construct()
    {
        parent::_construct();
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'sku' => array('ruleNotNullSKU', 'ruleUnique', 'ruleCustomDetailsView'),
                /** @see ruleNotNullName() */
                'name' => 'ruleNotNullName',
                'url_key' => array('ruleNotNull', 'ruleUrlValidate', 'ruleUnique'),
                'status' => 'ruleRevert',
                'inventory_sku' => 'ruleInventorySku',
            ),
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection->addAttributeToFilter('type_id', array('in' => array( 'bundle')));
        $productIds = $collection->getAllIds();

        // Bundle product names
        /** @var Mage_Eav_Model_Config $eavConfig */
        $eavConfig = Mage::getModel('eav/config');
        $entityTypeId = $eavConfig->getEntityType(Mage_Catalog_Model_Product::ENTITY)->getEntityTypeId();
        foreach (self::$_translatableFields as $code) {
            $attribute = $eavConfig->getAttribute(Mage_Catalog_Model_Product::ENTITY, $code);
            $backendTable = $attribute->getBackendTable();
            $this->_attributeCodeById[$attribute->getId()] = $code;
            /** @var Varien_Db_Adapter_Interface $connection */
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $select = $connection->select();
            $select->from(array('main_table' => $backendTable));
            $select->where('entity_type_id = ?', $entityTypeId);
            $select->where('attribute_id = ?', $attribute->getId());
            $select->where('store_id > 0');
            $select->where('entity_id IN (?)', $productIds);
            foreach ($connection->fetchAll($select) as $value) {
                $productId = $value['entity_id'];
                $attributeId = $value['attribute_id'];
                $storeId = $value['store_id'];
                if (!array_key_exists($storeId, $this->_storeLocales)) {
                    $this->_storeLocales[$storeId] = Mage::getStoreConfig('general/locale/code', $storeId);
                }

                if (!array_key_exists($productId, $this->_textFieldTranslations)) {
                    $this->_textFieldTranslations[$productId] = array();
                }
                $code = $this->_attributeCodeById[$attributeId];
                if (!array_key_exists($code, $this->_textFieldTranslations[$productId])) {
                    $this->_textFieldTranslations[$productId][$code] = array();
                }
                $this->_textFieldTranslations[$productId][$code][$storeId] = $value['value'];
            }
        }

        // Bundle item labels
        /** @var Mage_Bundle_Model_Resource_Option_Collection $options */
        $options = Mage::getResourceModel('bundle/option_collection');
        $options->setProductIdFilter(array('in' => $productIds));
        $options->getSelect()->join(
            array('option_value' => $options->getTable('bundle/option_value')),
            'main_table.option_id = option_value.option_id AND option_value.store_id > 0',
            array('*')
        );
        foreach ($options as $row) {
            $storeId = $row->getData('store_id');
            $productId = $row->getData('parent_id');
            $optionId = $row->getData('option_id');
            if (!array_key_exists($storeId, $this->_storeLocales)) {
                $this->_storeLocales[$storeId] = Mage::getStoreConfig('general/locale/code', $storeId);
            }
            if (!array_key_exists($productId, $this->_fieldLabelTranslations)) {
                $this->_fieldLabelTranslations[$productId] = array();
            }
            if (!array_key_exists($optionId, $this->_fieldLabelTranslations[$productId])) {
                $this->_fieldLabelTranslations[$productId][$optionId] = array();
            }
            $this->_fieldLabelTranslations[$productId][$optionId][$storeId] = $row->getData('title');
        }

        // Bundle item names
        /** @var Mage_Eav_Model_Config $eavConfig */
        $eavConfig = Mage::getModel('eav/config');
        $entityTypeId = $eavConfig->getEntityType(Mage_Catalog_Model_Product::ENTITY)->getEntityTypeId();
        $attr = $eavConfig->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'name');
        /** @var Varien_Db_Adapter_Interface $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select = $connection->select();
        $select->from(array('main_table' => $attr->getBackendTable()));
        $select->join(
            array('item' => $connection->getTableName('catalog_product_bundle_selection')),
            'main_table.entity_id = item.product_id',
            array('child_id' => 'product_id')
        );
        $select->where('entity_type_id = ?', $entityTypeId);
        $select->where('attribute_id = ?', $attr->getId());
        $select->where('store_id > 0');
        $select->where('item.parent_product_id IN (?)', $productIds);
        $select->group('main_table.value_id');
        foreach ($connection->fetchAll($select) as $value) {
            $productId = $value['entity_id'];
            $storeId = $value['store_id'];
            if (!array_key_exists($storeId, $this->_storeLocales)) {
                $this->_storeLocales[$storeId] = Mage::getStoreConfig('general/locale/code', $storeId);
            }
            if (!array_key_exists($productId, $this->_itemNameTranslations)) {
                $this->_itemNameTranslations[$productId] = array();
            }
            $this->_itemNameTranslations[$productId][$storeId] = $value['value'];
        }

    }

    /**
     * @inheritdoc
     */
    protected function _export()
    {
        $return = parent::_export();
        $processedProducts = (array)$this->_storage->getProductData('processed', 'products');
        $this->_storage->setProductData('processed', $this->_processed + $processedProducts, 'products');
        $this->_storage->setProductData('unique_keys', $this->_uniqueKeys, 'products');
        return $return;
    }

    /**
     * Set unique SKU if it's null
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleNotNullSKU(Varien_Object $model, $attribute)
    {
        $value = $model->getData($attribute);
        if (null === $value) {
            /** @var Mage_Core_Helper_Data $helper */
            $helper = Mage::helper('core');
            $value = $helper->getRandomString(12);
            $model->setData($attribute, $value);
        }
        return true;
    }

    /**
     * Generate attribute value from name if it's null.
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    /*public function ruleNotNull(Varien_Object $model, $attribute)
    {
        $value = $model->getData($attribute);
        if (null === $value) {
            echo 1;
            $value = $model->getData('sku') . "_" . $model->getData('name');
            $model->setData($attribute, $value);
        }
        return true;
    }*/

    /**
     * Set 'inventory_sku' from 'product_sku'
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleInventorySku(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, $model->getData('sku'));
        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleNotNullName(Varien_Object $model, $attribute)
    {
        $value = $model->getData($attribute);
        if (null === $value) {
            $value = $model->getData('sku');
            $model->setData($attribute, $value);
        }
        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleCustomDetailsView(Varien_Object $model, $attribute)
    {
        /** @var Mage_Bundle_Model_Product_Type $typeInstance */
        $typeInstance = $model->getTypeInstance();

        $optionCollection = $typeInstance->getOptionsCollection($model);

        $selectionCollection = $typeInstance->getSelectionsCollection(
            $typeInstance->getOptionsIds($model),
            $model
        );

        /** @var Mage_Bundle_Model_Option[] $fields */
        $fields = $optionCollection->appendSelections($selectionCollection, false);
        $result = array('frontend_fields' => array());
        $fieldFakeId = 1;

        foreach ($fields as $field) {
            /** @var Mage_Bundle_Model_Option $field */
            $type = 'select';
            if (isset($this->_typeMap[$field->getData('type')])) {
                $type = $this->_typeMap[$field->getData('type')];
            }

            $fieldData = array(
                'id' => $fieldFakeId,
                'name' => $field->getData('default_title'),
                'label' => $field->getData('default_title'),
                'input_type' => $type,
                'required' => $field->getData('required'),
                'options' => '',
                'option_serialized' => array(),
                'multilanguage' => array(),
            );
            $labels = $options = array();
            $position = 1;
            if (isset($this->_fieldLabelTranslations[$model->getId()][$field->getId()])) {
                $labelData = array();
                foreach ($this->_fieldLabelTranslations[$model->getId()][$field->getId()] as $storeId => $label) {
                    $labelData[$this->_storeLocales[$storeId]] = $label;
                }
                $fieldData['multilanguage']['name'] = $labelData;
                $fieldData['multilanguage']['label'] = $labelData;
            }

            foreach ($field->getData('selections') as $product) {
                /** @var Mage_Catalog_Model_Product $product */
                $name = preg_replace('/[^A-Za-z0-9_-]/', '', $product->getName());
                $labels[] = $name;
                $prices = array(
                    'id' => $model->getId(),
                    'product_id' => $model->getId(),
                    'customer_group_id' => '*',
                    'site_id' => '*',
                    'currency_code' => '*',
                    'price_type' => 'base',
                    'amount' => $product->getFinalPrice(),
                    'operation' => '=$',
                    'qty' => '1'
                );
                $options[$name] = array(
                    'sku' => $product->getSku(),
                    'position' => $position++,
                    'prices' => array((object)$prices),
                    'multilanguage' => array(),
                );
                if (isset($this->_itemNameTranslations[$product->getId()])) {
                    $nameData = array();
                    foreach ($this->_itemNameTranslations[$product->getId()] as $storeId => $storeValue) {
                        $nameData[$this->_storeLocales[$storeId]] = $storeValue;
                    }
                    $options[$name]['multilanguage'] = $nameData;
                }
            }
            $fieldData['options'] = implode(',', $labels);
            $fieldData['option_serialized'] = json_encode((object)$options);

            $result['frontend_fields'][] = $fieldData;
            $fieldFakeId++;
        }

        foreach (self::$_translatableFields as $sellvanaField => $magentoField) {
            if (isset($this->_textFieldTranslations[$model->getId()][$magentoField])) {
                $result[$sellvanaField] = '';
                $tmpArray = array();
                foreach ($this->_textFieldTranslations[$model->getId()][$magentoField] as $storeId => $storeValue) {
                    $tmpArray[] = array(
                        'value' => $storeValue,
                        'lang_code' => $this->_storeLocales[$storeId],
                        'input_type' => self::$_translatableFieldTypes[$sellvanaField]
                    );
                }
                $result[$sellvanaField] = json_encode($tmpArray);
            }
        }

        $model->setData('sellvana_custom_details_view', $result);
        $model->setData('sellvana_custom_data', $result);

        return true;
    }
}