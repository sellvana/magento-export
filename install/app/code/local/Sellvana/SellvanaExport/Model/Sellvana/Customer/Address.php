<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_Customer_Address
 */
class Sellvana_SellvanaExport_Model_Sellvana_Customer_Address extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName        = 'Sellvana_Customer_Model_Address';
    protected $_magentoModelName = 'customer/address';
    protected $_modelGroups      = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_CUSTOMER
    );
    protected $_uniqueKey        = array(
        'customer_id',
        'street1'
    );

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'                  => 'PK',//"1"
        'customer_id'         => 'parent_id',//"1"
        //'email'               => '',//"testemail@test.com"
        'firstname'           => 'firstname',//"firstname"
        'lastname'            => 'lastname',//"lastname"
        'middle_initial'      => 'middlename',//null
        'prefix'              => 'prefix',//null
        'suffix'              => 'suffix',//null
        'company'             => 'company',//null
        //'attn'                => '',//null
        'street1'             => 'street',//"test streat"
        //'street2'             => '',//"test streat"
        //'street3'             => '',//null
        'city'                => 'city',//"riga"
        'region'              => 'region',//"riga"
        'postcode'            => 'postcode',//"1069"
        'country'             => 'country_id',//"LV"
        'phone'               => 'telephone',//null
        'fax'                 => 'fax',//null
        'create_at'           => 'created_at',//"2015-10-12 09:07:02"
        'update_at'           => 'updated_at',//"2015-10-12 09:07:02"
        //'lat'                 => '',//null
        //'lng'                 => '',//null
        //'is_default_billing'  => '',//"0"
        //'is_default_shipping' => ''//"0"
    );

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'created_at' => 'ruleMysqlDate'
            )
        ));
    }
}