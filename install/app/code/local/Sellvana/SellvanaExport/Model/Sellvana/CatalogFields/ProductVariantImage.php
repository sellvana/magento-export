<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVariantImage
 */
class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductVariantImage
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_CatalogFields_Model_ProductVariantImage';
    protected $_magentoModelName  = 'catalog/product';
    protected $_modelGroups      = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_ATTRIBUTE,
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT
    );
    protected $_uniqueKey         = array(
        'product_id',
        'variant_id',
        'file_id'
    );

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'               => 'sellvana_autoincrement',
        'product_id'       => 'product_id',
        'variant_id'       => 'variant_id',
        'file_id'          => 'file_id',
        'product_media_id' => 'product_media_id',
        'position'         => 'position'
    );

    protected $_mediaAttributes = array();

    /** @var int */
    protected $_counter           = 0;

    public function _construct()
    {
        parent::_construct();

        /**
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable::_export,
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimple::_export
         */
        $this->setData('processed_products', $this->_storage->getProductData('processed', 'products'));

        $this->setData('variants', $this->_storage->getProductData('variants', 'products'));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection->addAttributeToFilter('type_id', array('in' => array('configurable')));
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'sellvana_autoincrement' => array('ruleVirtualAutoIncrement', 'ruleString')
            ),
            'skip' => 'skipProduct'
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareData(Varien_Object $model, $map = null)
    {
        $variants = $this->getData('variants');

        /** @var Mage_Catalog_Model_Product $model */
        $productId = $model->getId();

        /** @var Mage_Catalog_Model_Product_Type_Configurable $typeInstance */
        $typeInstance = $model->getTypeInstance();
        $children = $typeInstance->getUsedProductCollection();

        $return = array();
        foreach ($children as $child) {
            $this->_loadMediaGallery($child);

            $variantId = $variants[$child->getId()];
            $mediaGallery = $child->getData('media_gallery');
            foreach ($mediaGallery['images'] as $image) {

                $tmpModel = new Varien_Object();

                $fileId = $image['value_id'];

                $storageId = $this->_storage->getMediaLibraryData($image['file'], 'productImageFile');
                if (null !== $storageId) {
                    $fileId = $storageId;
                }

                $productMediaId = $this->_storage->getMediaLibraryData($productId . '/' . $storageId, 'productMediaId');

                $tmpModel->setData(array(
                    'product_id'       => $productId,
                    'variant_id'       => $variantId,
                    'file_id'          => $fileId,
                    'product_media_id' => $productMediaId,
                    'position'         => $image['position']
                ));

                $return[] = parent::_prepareData($tmpModel, $map);
            }
        }

        if (($this->_counter++ % 50) == 0) {
            gc_collect_cycles();
        }

        return $return;
    }


    /**
     * @param Mage_Catalog_Model_Product $model
     * @return bool
     */
    public function skipProduct(Mage_Catalog_Model_Product $model)
    {
        $processedProducts = $this->getData('processed_products');

        $id = $model->getId();
        return !(
            array_key_exists($id, $processedProducts)
            && $processedProducts[$id] == 'configurable'
        );
    }

    /**
     * @param Varien_Object $model
     * @throws Mage_Core_Exception
     */
    protected function _loadMediaGallery(Varien_Object $model)
    {
        $attributeSetId = $model->getAttributeSetId();

        if (!isset($this->_mediaAttributes[$attributeSetId])) {
            $this->_mediaAttributes[$attributeSetId] = $model->getTypeInstance(true)->getSetAttributes($model);
        }

        $key = 'media_gallery';
        /** @var Mage_Catalog_Model_Resource_Eav_Attribute $mediaGallery */
        $mediaGallery = $this->_mediaAttributes[$attributeSetId][$key];
        $backend = $mediaGallery->getBackend();
        $backend->afterLoad($model);
    }

}