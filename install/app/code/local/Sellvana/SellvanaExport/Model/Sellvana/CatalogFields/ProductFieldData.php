<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductFieldData
 */
class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_ProductFieldData
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName        = 'Sellvana_CatalogFields_Model_ProductFieldData';
    protected $_magentoModelName = 'catalog/product';
    protected $_modelGroups      = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_ATTRIBUTE,
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT
    );
    protected $_uniqueKey        = array(
        'product_id',
        'field_id',
        'site_id'
    );

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'         => 'PK',
        'product_id' => 'selvana_product_id',
        'set_id'     => 'selvana_set_id',
        'field_id'   => 'selvana_field_id',
        //'position'   => '',
        'site_id'    => 'selvana_site_id',//TODO: need realization
        //'locale'     => '',
        'value_id'   => 'selvana_value_id',//PK of FieldOption
        'value_int'  => 'selvana_value_int',
        'value_dec'  => 'selvana_value_dec',
        'value_var'  => 'selvana_value_var',
        'value_text' => 'selvana_value_text',
        'value_date' => 'selvana_value_date'
    );

    /**
     * @inheritdoc
     */
    public function _construct()
    {
        parent::_construct();

        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_Field::_export */
        $this->setData('field_data', $this->_storage->getCatalogFieldsData('attribute_codes', 'field'));

        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_SetField::_export */
        $this->setData('group_ids', $this->_storage->getCatalogFieldsData('attribute_codes', 'setfield'));

        /**
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable::_export,
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimple::_export
         */
        $this->setData('processed_products', $this->_storage->getProductData('processed', 'products'));

        /** @see Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_FieldOption::_export */
        $this->setData('option_map', $this->_storage->getCatalogFieldsData('option_map', 'field_options'));
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'PK'                 => array('ruleVirtualAutoIncrement', 'ruleString'),
                'selvana_product_id' => 'ruleString',
                'selvana_set_id'     => 'ruleString',
                'selvana_field_id'   => 'ruleString',
                'selvana_value_id'   => 'ruleAttributeValue',
                'selvana_value_int'  => 'ruleAttributeValue',
                'selvana_value_dec'  => 'ruleAttributeValue',
                'selvana_value_var'  => 'ruleAttributeValue',
                'selvana_value_text' => 'ruleAttributeValue',
                'selvana_value_date' => 'ruleAttributeValue'
            ),
            'skip' => array()
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _export()
    {
        /** @var  Mage_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel($this->_magentoModelName);

        //$model = Mage::getModel('sellvana_sellvanaexport/sellvana_virtual');
        $model = new Varien_Object();

        $fieldData         = $this->getData('field_data');
        $groupIds          = $this->getData('group_ids');
        $processedProducts = $this->getData('processed_products');
        $optionMap         = $this->getData('option_map');

        $counter = 0;
        $offsetSize = $this->_selectOffsetSize;
        $attributes = array();
        $productTypes = array();

        $attributeCollection = Mage::getModel('eav/config')
            ->getEntityType(Mage_Catalog_Model_Product::ENTITY)
            ->getAttributeCollection()
            ->addFieldToFilter('is_user_defined', 1);
        foreach ($attributeCollection as $attr) {
            $attributes[] = $attr->getAttributeCode();
            if ($attr->getData('apply_to')) {
                $productTypes[$attr->getAttributeCode()] = $attr->getData('apply_to');
            }
        }

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = $productModel->getCollection();
        $collection->addAttributeToSelect($attributes);
        $select = $collection->getSelect();
        $select->limit($offsetSize, $offsetSize * $counter++);

        if ($collection->count() == 0) {
            return null;
        }

        while ($collection->count() > 0) {
            /** @var Mage_Catalog_Model_Product $productModel */
            foreach ($collection as $productModel) {
                $productId = $productModel->getID();

                //skip skipped products
                if (!array_key_exists($productId, $processedProducts)) {
                    continue;
                }

                $attributeData = $productModel->getData();
                foreach ($attributeData as $attribute => $data) {

                    if ($data === null || !array_key_exists($attribute, $fieldData)) {
                        continue;
                    }

                    if (!empty($productTypes[$attribute])) {
                        $applyTo = explode(',', $productTypes[$attribute]);

                        if (!in_array($productModel->getTypeId(), $applyTo)) {
                            continue;
                        }
                    }

                    $attributeId = $fieldData[$attribute]['id'];
                    $attributeType = $fieldData[$attribute]['type'];
                    $attributeGroupId = $groupIds[$attributeId];

                    //skip attributes with unknown type and static;
                    if (null === $attributeType || $attributeType == 'static') {
                        continue;
                    }

                    $model->setData(array(
                        'selvana_product_id' => $productId,
                        'selvana_set_id'     => $attributeGroupId,
                        'selvana_field_id'   => $attributeId,
                        'attribute_code'     => $attribute,
                        'value'              => $data
                    ));

                    if ($attributeType == 'options') {
                        $values = explode(',', $data);
                        foreach ($values as $value) {
                            $value = $optionMap[$attribute . '/' . $value];
                            $model->setData('value', $value);

                            $modelData = $this->_prepareData($model);
                            $this->writeToFile($modelData);
                        }
                    } else {
                        $modelData = $this->_prepareData($model);
                        $this->writeToFile($modelData);
                    }
                }
            }

            $collection->clear();
            $select->limit($offsetSize, $offsetSize * $counter++);
        }

        return $this;
    }

    /**
     * Set value of attribute to field by it type
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleAttributeValue(Varien_Object $model, $attribute)
    {
        $fieldData = $this->getData('field_data');
        $attributeCode = $model->getData('attribute_code');
        $attributeType = $fieldData[$attributeCode]['type'];
        $data = $model->getData('value');

        $value = null;
        switch ($attributeType) {
            case 'options':
                $value = $attribute == 'selvana_value_id' ? $data : null;
                break;
            case 'text':
                $value = $attribute == 'selvana_value_text' ? $data : null;
                break;
            case 'tinyint':
            case 'int':
                $value = $attribute == 'selvana_value_int' ? $data : null;
                break;
            case 'varchar':
                $value = $attribute == 'selvana_value_var' ? $data : null;
                break;
            case 'decimal':
                $value = $attribute == 'selvana_value_dec' ? $data : null;
                break;
            case 'datetime':
                $value = $attribute == 'selvana_value_date' ? $data : null;
                break;
            case 'static':
            default:
                return false;
                break;
        }

        $model->setData($attribute, $value);
        return true;
    }
}