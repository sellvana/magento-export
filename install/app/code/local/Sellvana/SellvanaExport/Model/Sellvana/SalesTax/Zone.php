<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_SalesTax_Zone
 */
class Sellvana_SellvanaExport_Model_Sellvana_SalesTax_Zone extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName        = 'Sellvana_SalesTax_Model_Zone';
    protected $_magentoModelName = 'tax/calculation_rate';
    protected $_modelGroups      = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_TAX
    );
    protected $_uniqueKey        = array(
        'zone_type',
        'title',
        'postcode_from',
        'postcode_to'
    );

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'                => 'PK',//"1"
        'zone_type'         => 'sellvana_zone_type',//"region"
        'title'             => 'code',//ask: title is multilingual field, so it does not exist in collection
        'country'           => 'tax_country_id',//"US"
        'region'            => 'tax_region_id',//"NY"
        'postcode_from'     => 'sellvana_postcode_from',//null
        'postcode_to'       => 'sellvana_postcode_to',//null
        'zone_rate_percent' => 'rate',//null
        //'create_at'         => '',//"2015-10-07 11:49:26"
        //'update_at'         => ''//"2015-10-07 11:49:26"
    );

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'sellvana_zone_type' => 'ruleZoneType',
                'tax_region_id' => 'ruleRegionId',
                'sellvana_postcode_from' => 'rulePostcodeFrom',
                'sellvana_postcode_to' => 'rulePostcodeTo'
            ),
            'skip' => 'skipZone'
        ));
    }

    /**
     * @param Varien_Object $model
     * @return bool
     */
    public function skipZone(Varien_Object $model)
    {
        $skip = !$this->ruleZoneType(clone $model, 'sellvana_zone_type');

        if ($skip) {
            $this->_storage->setData($model->getId(), true, 'sales_tax', 'zone');
        }

        return $skip;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleRegionId(Varien_Object $model, $attribute)
    {
        $regionId = $model->getData($attribute);
        if ($regionId === 0) {
            $model->setData($attribute, null);
        } else {
            $region = Mage::getModel('directory/region')->load($regionId);
            $model->setData($attribute, $region->getData('code'));
        }
        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleZoneType(Varien_Object $model, $attribute)
    {
        $region = $model->getData('tax_region_id');
        $postcode = $model->getData('tax_postcode');

        $isRange = $model->getData('zip_is_range');

        //TODO: think about what to do with a mask in the postcode
        if (strpos($postcode, '*', 1) !== false) {
            return false;
        }

        $zoneType = null;

        if ($isRange == false) {
            if ($postcode == '*') {
                $zoneType = ($region == 0 ? 'country' : 'region');
            } else {
                $zoneType = 'postcode';
            }
        } else {
            $zoneType = 'postrange';
        }

        if (null !== $zoneType) {
            $model->setData($attribute, $zoneType);
            return true;
        }

        return false;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function rulePostcodeFrom(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, $this->_getPostcode($model, 'zip_from'));
        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function rulePostcodeTo(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, $this->_getPostcode($model, 'zip_to'));
        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return mixed|null
     */
    protected function _getPostcode(Varien_Object $model,$attribute)
    {
        switch ($model->getData('sellvana_zone_type')){
            case 'postcode':
                return $model->getData('tax_postcode');
                break;
            case 'postrange':
                return $model->getData($attribute);
                break;
            case 'country':
            case 'region':
            default:
                return null;
                break;
        }
    }
}