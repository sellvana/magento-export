<?php
/* @var $this Mage_Core_Model_Resource_Setup */

$this->startSetup();

$table = $this->getTable('sellvana_sellvanaexport/profile');

$this->getConnection()->addColumn($table, 'last_response', 'datetime NULL');

$this->endSetup();