<?php
/* @var $this Mage_Core_Model_Resource_Setup */

$this->startSetup();

$table = $this->getTable('sellvana_sellvanaexport/profile');

$this->getConnection()->addColumn($table, 'run_status', 'varchar(16) NULL DEFAULT "' . Sellvana_SellvanaExport_Model_Profile::STATUS_NEW . '"');

$this->endSetup();