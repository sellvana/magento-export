<?php
/* @var $this Mage_Core_Model_Resource_Setup */

$this->startSetup();

$table = $this->getTable('sellvana_sellvanaexport/profile');

$this->getConnection()->addColumn($table, 'description', 'mediumtext NULL AFTER `file`');

$this->getConnection()->addIndex(
        $table,
        $this->getIdxName('sellvana_sellvanaexport/profile', array('file')),
        array('file'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    );

$this->endSetup();